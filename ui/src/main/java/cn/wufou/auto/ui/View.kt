package cn.wufou.auto.ui

import android.view.View
import androidx.core.view.doOnAttach
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

/**
 * 等待View附加上去
 */
suspend fun View.awaitAttachView(): View {
    return suspendCoroutine<View> { callback ->
        this.doOnAttach { view ->
            callback.resume(view)
        }
    }
}