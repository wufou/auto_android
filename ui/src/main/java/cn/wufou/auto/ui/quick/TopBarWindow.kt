package cn.wufou.auto.ui.quick

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import cn.wufou.auto.ui.R
import cn.wufou.auto.ui.UIClient
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext


abstract class TopBarWindow(private val context: Context, private val uiClient: UIClient, private val onCloseCallback: (() -> Unit)? = null) {

    private var view: View? = null

    /**
     * 打开悬浮窗
     */
    suspend fun open() {
        val view = withContext(Dispatchers.Main) {
            val view = LayoutInflater.from(context).inflate(R.layout.sample_top_bar_window, null, false)
            uiClient.addUIAwait(view, ViewGroup.LayoutParams.MATCH_PARENT)
            view.findViewById<View>(R.id.stop)?.setOnClickListener {
                uiClient.removeUI(view)
                onCloseCallback?.invoke()
            }
            view
        }
        this.view = view
    }

    /**
     * 关闭悬浮窗
     */
    fun close() {
        view?.apply {
            uiClient.removeUI(this)
            onCloseCallback?.invoke()
        }
        view = null
    }

    /**
     * 修改提示信息
     */
    fun setMessage(message: String) {
        view?.findViewById<TextView>(R.id.title)?.apply {
            setTextColor(Color.BLACK)
            text = message
        }
        view?.findViewById<View>(R.id.loading)?.visibility = View.VISIBLE
    }


    /**
     * 修改错误信息
     */
    fun setError(message: String) {
        view?.findViewById<TextView>(R.id.title)?.apply {
            setTextColor(Color.RED)
            text = message
        }
        view?.findViewById<View>(R.id.loading)?.visibility = View.INVISIBLE
    }

    /**
     * 是否启用遮罩层
     */
    fun enableMask(enable: Boolean) {
        val maskView = view?.findViewById<View>(R.id.mask) ?: return
        view?.apply {
            val lp = layoutParams
            if (enable) {
                lp.height = ViewGroup.LayoutParams.MATCH_PARENT
            } else {
                lp.height = ViewGroup.LayoutParams.WRAP_CONTENT
            }
            uiClient.updateUILayout(this, lp)
        }
        if (enable) {
            maskView.visibility = View.VISIBLE
        } else {
            maskView.visibility = View.GONE
        }
    }


}