package cn.wufou.auto.ui

import android.content.Context
import android.graphics.PixelFormat
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.FrameLayout
import androidx.core.view.doOnAttach
import cn.wufou.auto.core.MagicCore

/**
 * UI上下文
 */
class UIContext {

    private fun getContext(): Context? {
        return MagicCore.getServiceContext()
    }

    private var container: FrameLayout? = null


    /**
     * 添加一个元素到屏幕上
     */
    fun addView(view: View,  width: Int = WindowManager.LayoutParams.WRAP_CONTENT, height: Int = WindowManager.LayoutParams.WRAP_CONTENT) {
        val context = getContext()
        if (context == null) {
            // 忽略
            Log.e("ScreenLayout", "服务未启动的时候尝试添加一个悬浮界面！")
            return
        }
        val windowManager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val lp = WindowManager.LayoutParams().apply {
            this.width = width
            this.height =height
            this.type = WindowManager.LayoutParams.TYPE_ACCESSIBILITY_OVERLAY
            this.format = PixelFormat.TRANSPARENT
            this.gravity = Gravity.START or Gravity.TOP
            this.flags = WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL or WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
        }
//        val container = this.container ?: FrameLayout(context)
//        container.addView(view, index, FrameLayout.LayoutParams(width, height))
//        if (this.container == null) {
            windowManager.addView(view, lp)
//            this.container = container
//        }
    }

    fun updateUILayout(view: View,layoutParams: ViewGroup.LayoutParams) {
        val context = getContext()
        if (context == null) {
            // 忽略
            Log.e("ScreenLayout", "服务未启动的时候尝试添加一个悬浮界面！")
            return
        }
        val windowManager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        windowManager.updateViewLayout(view,layoutParams)

    }
    fun removeView(view: View) {
        val context = getContext()
        if (context == null) {
            // 忽略
            Log.e("ScreenLayout", "服务未启动的时候尝试添加一个悬浮界面！")
            return
        }
        val windowManager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        windowManager.removeView(view)
    }

}