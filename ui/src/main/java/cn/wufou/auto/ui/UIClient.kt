package cn.wufou.auto.ui

import android.view.View
import android.view.ViewGroup
import android.view.WindowManager

class UIClient {

    private val context = UIContext()

    fun addUI(view: View, width: Int = WindowManager.LayoutParams.WRAP_CONTENT, height: Int = WindowManager.LayoutParams.WRAP_CONTENT) {
        context.addView(view, width, height)
    }

    suspend fun addUIAwait(view: View, width: Int = WindowManager.LayoutParams.WRAP_CONTENT, height: Int = WindowManager.LayoutParams.WRAP_CONTENT) {
        this.addUI(view, width, height)
        view.awaitAttachView()
    }

    fun updateUILayout(view: View, layoutParams: ViewGroup.LayoutParams) {
        context.updateUILayout(view, layoutParams)
    }

    fun removeUI(view: View) {
        try {
            context.removeView(view)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


}