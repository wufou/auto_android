package cn.wufou.auto.engine.modules

import cn.wufou.auto.engine.modules.action.impl.BasicActionModule
import cn.wufou.auto.engine.modules.gesture.impl.BasicGestureModule
import cn.wufou.auto.engine.modules.node.basic.BasicNodeModule
import cn.wufou.auto.engine.modules.toast.basic.BasicToastModule
import cn.wufou.auto.engine.modules.window.impl.BasicWindowModule
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

/**
 * 模块适配
 */
internal object ModuleAdapter : IModuleAdapter {

    /**
     * 窗口管理
     */
    override val window by setup { BasicWindowModule(this) }

    /**
     * 节点管理
     */
    override val node by setup { BasicNodeModule() }

    /**
     * 手势管理
     */
    override val gesture by setup { BasicGestureModule() }


    /**
     * Toast管理
     */
    override val toast by setup { BasicToastModule() }

    /**
     * 行为管理
     */
    override val action by setup { BasicActionModule() }

    /**
     * 设置模块
     */
    private fun <T> setup(factory: () -> T): ReadOnlyProperty<ModuleAdapter, T> {

        return object : ReadOnlyProperty<ModuleAdapter, T> {
            var value: T? = null
            override fun getValue(thisRef: ModuleAdapter, property: KProperty<*>): T {
                val value = this.value
                if (value != null) {
                    return value
                }
                return factory.invoke().also {
                    this.value = it
                }
            }

        }
    }
}