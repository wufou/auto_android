package cn.wufou.auto.engine.feature.layer

import android.graphics.Rect

/**
 * UI层节点描述
 */
interface LayerInfo {

    /**
     * 屏幕区域
     */
    val rect: Rect

    /**
     * 排序
     */
    val order: Int


    /**
     * 子节点
     */
    val children: List<LayerInfo>

    /**
     * 刷新加载
     */
    fun refresh()
}