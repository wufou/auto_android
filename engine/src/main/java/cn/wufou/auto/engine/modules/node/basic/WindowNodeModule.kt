package cn.wufou.auto.engine.modules.node.basic

import cn.wufou.auto.engine.component.Component
import cn.wufou.auto.engine.component.ComponentFinder
import cn.wufou.auto.engine.modules.node.NodeModule
import cn.wufou.auto.engine.modules.node.entity.NodeInfo
import cn.wufou.auto.engine.modules.node.toNodeInfo
import cn.wufou.auto.engine.modules.window.entity.WindowInfo
import kotlinx.coroutines.currentCoroutineContext
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive

/**
 * 基于某个Window的查询
 */
class WindowNodeModule(private val windowInfo: WindowInfo) : NodeModule {

    override fun getCurrentRootNodeInfo(): NodeInfo? {
        return windowInfo.getRootNodeInfo()
    }

    override suspend fun awaitCurrentRootNodeInfo(): NodeInfo {
        while (currentCoroutineContext().isActive) {
            val root = getCurrentRootNodeInfo()
            if (root != null) {
                return root
            }
        }
        throw RuntimeException("没有找到焦点窗口根节点")
    }


    override fun checkFeature(onlyVisibleToUser: Boolean, vararg featureFilter: (NodeInfo) -> Boolean): Boolean {
        return windowInfo.getRootNodeInfo()?.checkFeature(onlyVisibleToUser, *featureFilter) ?: false
    }

    override fun text(text: String): List<NodeInfo> {
        return windowInfo.getRootNodeInfo()?.getSource()?.findAccessibilityNodeInfosByText(text)?.map { it.toNodeInfo() } ?: emptyList()
    }

    override suspend fun await(timeout: Long, error: Throwable, block: (NodeInfo?) -> Boolean) {
        val startTime = System.currentTimeMillis()
        while (currentCoroutineContext().isActive) {
            if (System.currentTimeMillis() - startTime >= timeout) {
                throw error
            }
            val root = windowInfo.getRootNodeInfo()
            root?.refresh()
            if (block.invoke(root)) {
                return
            }
            delay(300)
        }
        throw RuntimeException("任务已中断")
    }

    override suspend fun findOne(index: Int, onlyVisibleToUser: Boolean, filter: (NodeInfo) -> Boolean): NodeInfo? {
        return windowInfo.getRootNodeInfo()?.findOne(index, onlyVisibleToUser, filter)
    }

    override suspend fun <C : Component, F : ComponentFinder<C, NodeInfo>> findComponent(finder: Class<F>): C? {
        return windowInfo.getRootNodeInfo()?.run { ComponentFinder.find(finder, this) }
    }


    override suspend fun <C : Component, F : ComponentFinder<C, NodeInfo>> findComponentAwait(finder: Class<F>, timeout: Long, error: Throwable): C {
        val startTime = System.currentTimeMillis();
        while (currentCoroutineContext().isActive) {
            if (System.currentTimeMillis() - startTime >= timeout) {
                throw error
            }
            val component = findComponent(finder)
            if (component != null) {
                return component
            }
            delay(300)
        }
        throw RuntimeException("任务已中断")
    }

    override suspend fun findOneAwait(timeout: Long, error: Throwable, block: (NodeInfo?) -> NodeInfo?): NodeInfo {
        val startTime = System.currentTimeMillis()
        while (currentCoroutineContext().isActive) {
            if (System.currentTimeMillis() - startTime >= timeout) {
                throw error
            }
            val root = windowInfo.getRootNodeInfo()
            root?.refresh()
            val result = block.invoke(root)
            if (result != null) {
                return result
            }
            delay(300)
        }
        throw RuntimeException("任务已中断")
    }


}