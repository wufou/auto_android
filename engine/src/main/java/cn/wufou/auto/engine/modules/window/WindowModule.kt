package cn.wufou.auto.engine.modules.window

import cn.wufou.auto.engine.modules.window.entity.WindowInfo

/**
 * window模块
 */
interface WindowModule {


    /**
     * 获取所有窗口信息
     */
    fun windowInfoList(): List<WindowInfo>


    /**
     * 获取窗口信息
     */
    fun applicationWindowInfoList(): List<WindowInfo>


}