package cn.wufou.auto.engine

import android.app.Application
import androidx.lifecycle.*
import cn.wufou.auto.core.MagicCore
import cn.wufou.auto.engine.modules.EngineContext
import cn.wufou.auto.engine.modules.IModuleAdapter
import cn.wufou.auto.engine.modules.ModuleAdapter

/**
 * 定义自动化引擎，提供以下功能
 * * 启动、关闭无障碍服务
 * * 为自动化脚本提供上下文环境
 *
 */
object AutoEngine {

    private val enableCallbacks: MutableMap<Int, LifecycleEventObserver> = mutableMapOf()


    /**
     * 应用上下文
     */
    fun getApplication(): Application {
        return MagicCore.getApplication()
    }

    /**
     * 设置前台通知的标题
     */
    fun setForegroundNotifyTitle(title: CharSequence) {
        MagicCore.setForegroundNotifyTitle(title)
    }

    /**
     * 设置前台通知的文本内容
     */
    fun setForegroundNotifyText(text: CharSequence) {
        MagicCore.setForegroundNotifyText(text)
    }


    /**
     * 设置前台通知的小图标
     */
    fun setForegroundNotifySmallIcon(smallIcon: Int) {
        MagicCore.setForegroundNotifySmallIcon(smallIcon)
    }


    /**
     * 引擎是否已经启动，引擎启动不代表脚本在运行，一个引擎理论上可以同时执行多个脚本
     */
    @Deprecated("已废弃", ReplaceWith("MagicCore.isEngineEnable()", "cn.wufou.auto.core.MagicCore"))
    fun isEnable(): Boolean {
        return MagicCore.isEnable()
    }

    /**
     * 引擎是否已经启动，引擎启动不代表脚本在运行，一个引擎理论上可以同时执行多个脚本
     */
    fun isEngineEnable(): Boolean {
        return isEnable()
    }


    /**
     * 引擎是否已经启动，引擎启动不代表脚本在运行，一个引擎理论上可以同时执行多个脚本
     */
    fun isEngineEnableLiveData(): LiveData<Boolean> {
        return object : LiveData<Boolean>() {
            var cbId: Int? = null
            override fun onActive() {
                cbId = addEnableCallback { postValue(it) }
            }

            override fun onInactive() {
                cbId?.apply { removeEnableCallback(this) }
                cbId = null
            }
        }

    }


    /**
     * 设置一个一次性的监听器，当引擎被启动后触发，触发后移除监听
     * @param callback 启动后被调用
     */
    fun setupOnceEnableCallback(callback: () -> Unit) {
        MagicCore.lifecycle.addObserver(object : LifecycleEventObserver {
            override fun onStateChanged(source: LifecycleOwner, event: Lifecycle.Event) {
                if (event == Lifecycle.Event.ON_START) {
                    // 当启动后自动移除监听
                    source.lifecycle.removeObserver(this)
                    callback.invoke()
                }
            }
        })
    }

    /**
     * 添加引擎启用状态监听器
     */
    fun addEnableCallback(callback: (Boolean) -> Unit): Int {
        val observer = LifecycleEventObserver { _, event ->
            if (event == Lifecycle.Event.ON_START) {
                // 当启动后自动移除监听
                callback.invoke(true)
            } else if (event == Lifecycle.Event.ON_STOP) {
                callback.invoke(false)
            }
        }
        MagicCore.lifecycle.addObserver(observer)
        val cbId = observer.hashCode()
        enableCallbacks[cbId] = observer
        return cbId
    }

    /**
     * 移除引擎启用状态监听器
     */
    fun removeEnableCallback(cbId: Number) {
        enableCallbacks.remove(cbId)
    }


    /**
     * 启动服务
     * @return true:当要去启动服务时，这只代表要去启动服务不代表服务一定能启动。false:当服务已经启动时
     */
    @Deprecated("已废弃")
    fun start(callback: (() -> Unit)? = null): Boolean {
        if (callback != null) {
            setupOnceEnableCallback(callback)
        }
        return MagicCore.start()
    }

    /**
     * 启动引擎
     * @return true:当要去启动服务时，这只代表要去启动服务不代表服务一定能启动。false:当服务已经启动时
     */
    fun startEngine(callback: (() -> Unit)? = null): Boolean {
        return start(callback)
    }


    /**
     * 停止服务
     * @return true:关闭了服务。false:当服务本来就没有启动时
     */
    @Deprecated("已废弃")
    fun stop(): Boolean {
        return MagicCore.stop()
    }


    /**
     * 停止引擎
     * @return true:关闭了服务。false:当服务本来就没有启动时
     */
    fun stopEngine(): Boolean {
        return stop()
    }


    /**
     * 模块适配器
     */
    @Deprecated("已废弃", ReplaceWith("EngineContext", "cn.wufou.auto.engine.modules.EngineContext"))
    fun getModuleAdapter(): IModuleAdapter {
        return ModuleAdapter
    }


    /**
     * 获取引擎上下文
     */
    @Deprecated("已废弃", ReplaceWith("EngineContext", "cn.wufou.auto.engine.modules.EngineContext"))
    fun geEngineContext(): EngineContext {
        return ModuleAdapter
    }

    /**
     * 获取引擎上下文
     */
    fun getEngineContext(): EngineContext {
        return ModuleAdapter
    }

}