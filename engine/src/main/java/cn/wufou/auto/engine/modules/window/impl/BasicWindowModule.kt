package cn.wufou.auto.engine.modules.window.impl

import android.view.accessibility.AccessibilityWindowInfo
import cn.wufou.auto.core.MagicCore
import cn.wufou.auto.engine.modules.IModuleAdapter
import cn.wufou.auto.engine.modules.window.WindowModule
import cn.wufou.auto.engine.modules.window.entity.WindowInfo
import cn.wufou.auto.engine.modules.window.toWindowInfo

class BasicWindowModule(private val adapter: IModuleAdapter) : WindowModule {

    private val magic = MagicCore.getMagicClient()


    override fun windowInfoList(): List<WindowInfo> {
        return magic.getWindows().map { it.toWindowInfo() }
    }

    override fun applicationWindowInfoList(): List<WindowInfo> {
        return magic.getWindows().filter { it.type == AccessibilityWindowInfo.TYPE_APPLICATION }.map { it.toWindowInfo() }
    }

}