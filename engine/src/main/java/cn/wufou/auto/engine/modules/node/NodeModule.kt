package cn.wufou.auto.engine.modules.node

import cn.wufou.auto.engine.component.Component
import cn.wufou.auto.engine.component.ComponentFinder
import cn.wufou.auto.engine.modules.node.entity.NodeInfo

/**
 * 节点模块
 */
interface NodeModule {


    /**
     * 获取当前获取焦点的根节点
     */
    fun getCurrentRootNodeInfo(): NodeInfo?

    /**
     * 获取当前获取焦点的根节点
     */
    suspend fun awaitCurrentRootNodeInfo(): NodeInfo

    /**
     * 检查当前页面是否符合特征
     */
    fun checkFeature(onlyVisibleToUser: Boolean = true, vararg featureFilter: (NodeInfo) -> Boolean): Boolean

    /**
     * 找到符合条件的
     */
    fun text(text: String): List<NodeInfo>

    /**
     * 等待一个节点
     */
    suspend fun await(timeout: Long = 5000, error: Throwable = RuntimeException("等待节点超时"), block: (NodeInfo?) -> Boolean)

    /**
     * 查询一个指定条件的节点
     */
    suspend fun findOneAwait(timeout: Long = 5000, error: Throwable = RuntimeException("查询节点超时"), block: (NodeInfo?) -> NodeInfo?): NodeInfo

    /**
     * 找到指定顺序符合条件的节点
     * @param index 节点序号 第几个符合条件的节点，在某些有多个类似元素的场景下可能会用到
     * @param onlyVisibleToUser 是否是用户可见的节点
     * @param filter 过滤函数
     */
    suspend fun findOne(index: Int = 0, onlyVisibleToUser: Boolean = true, filter: (NodeInfo) -> Boolean): NodeInfo?


    /**
     * 组件是一个节点和子节点的集合信息
     * 查找页面组件
     */
    suspend fun <C : Component, F : ComponentFinder<C, NodeInfo>> findComponent(finder: Class<F>): C?


    /**
     * 组件是一个节点和子节点的集合信息
     * 查找页面组件
     */
    suspend fun <C : Component, F : ComponentFinder<C, NodeInfo>> findComponentAwait(finder: Class<F>, timeout: Long, error: Throwable): C

}