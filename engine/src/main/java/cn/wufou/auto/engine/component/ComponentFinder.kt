package cn.wufou.auto.engine.component


/**
 * 定义查找器
 */
interface ComponentFinder<C : Component, S> {


    companion object {

        /**
         * 使用模块适配器去寻找窗口上的组件
         */
        suspend fun <C, S, T : ComponentFinder<C, S>> find(finder: Class<T>, node: S): C? {
            return finder.newInstance().find(node)
        }


    }

    /**
     * 嗅探匹配条件的元素
     */
    suspend fun find(context: S): C?



}