package cn.wufou.auto.engine.scene

import cn.wufou.auto.engine.modules.EngineContext
import kotlinx.coroutines.currentCoroutineContext
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive

/**
 * 执行符合条件的场景
 * @param timeout 超时时间(ms)
 * @param timeoutError 超时抛出的异常
 * @param scenes 执行场景（按序）
 */
suspend fun EngineContext.conditionScene(timeout: Long, timeoutError: Exception, vararg scenes: ConditionScene): Boolean {
    val startTime = System.currentTimeMillis()
    while (currentCoroutineContext().isActive) {
        if (System.currentTimeMillis() - startTime > timeout) {
            throw  timeoutError
        }
        for (secondScent in scenes) {
            if (secondScent.condition(this)) {
                return secondScent.execute(this)
            }
        }
        delay(300)
    }
    return false
}

//
///**
// * 执行符合条件的场景
// * @param count 异常最大次数
// * @param timeoutError 超时抛出的异常
// * @param scenes 执行场景（按序）
// */
//suspend fun EngineContext.errorScene(maxErrorCount: Int = 5, maxErrorCountError: Exception, scene: ConditionScene, vararg errorScenes: ConditionScene): Boolean {
//    var errorCount = 0
//    if (scene.condition(this)) {
//        if (scene.execute(this)) {
//            return true
//        } else {
//            for (errorScent in errorScenes) {
//                if (errorScent.condition(this)) {
//                    errorCount++
//                }
//            }
//        }
//    }
//    return false
//}


