package cn.wufou.auto.engine.modules

import cn.wufou.auto.engine.modules.action.ActionModule
import cn.wufou.auto.engine.modules.gesture.GestureModule
import cn.wufou.auto.engine.modules.node.NodeModule
import cn.wufou.auto.engine.modules.toast.ToastModule
import cn.wufou.auto.engine.modules.window.WindowModule

/**
 * 自动化引擎上下文环境
 */
interface EngineContext {
    /**
     * 窗口管理
     */
    val window: WindowModule

    /**
     * 节点管理
     */
    val node: NodeModule

    /**
     * 手势管理
     */
    val gesture: GestureModule

    /**
     * toast管理
     */
    val toast: ToastModule

    /**
     * 行为管理
     */
    val action: ActionModule
}