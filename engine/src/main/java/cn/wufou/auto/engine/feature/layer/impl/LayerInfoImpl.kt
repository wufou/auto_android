package cn.wufou.auto.engine.feature.layer.impl

import android.graphics.Rect
import android.util.Log
import androidx.core.view.accessibility.AccessibilityNodeInfoCompat
import cn.wufou.auto.engine.feature.layer.LayerInfo
import cn.wufou.auto.engine.feature.layer.LayerInfoBuilder

class LayerInfoImpl(private val source: AccessibilityNodeInfoCompat) : LayerInfo {


    override val rect: Rect = Rect()
    override var order: Int = 0
        private set
    private val mChildren = mutableListOf<LayerInfo>()
    override val children: List<LayerInfo>
        get() = mChildren.toList()

    override fun refresh() {
        source.getBoundsInScreen(rect)
        this.order = source.drawingOrder
        this.loadChildren()

    }

    private fun loadChildren() {
        val childNodeList = mutableListOf<AccessibilityNodeInfoCompat>()
        val count = source.childCount
        for (index in 0 until count) {
            try {
                val child = source.getChild(index)
                if (child != null) {
                    childNodeList.add(child)
                } else {
                    // 已经发生了变更
                    Log.w("LayerInfoImpl", "某些子节点已经发生了变化")
                }
            } catch (e: Exception) {
                Log.e("LayerInfoImpl", "某些子节点已经发生了变化", e)
            }
        }
        val children = childNodeList.map {
            LayerInfoBuilder(it).build().apply {
                try {
                    this.refresh()
                } catch (e: Exception) {
                    Log.e("LayerInfoImpl", "刷新子节点失败", e)
                }
            }
        }
        mChildren.clear()
        mChildren.addAll(children)
    }


}