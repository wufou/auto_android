package cn.wufou.auto.engine.modules.action

import android.annotation.TargetApi
import android.content.ComponentName
import android.content.Intent
import android.os.Build

/**
 * 定义行为操作
 */
interface ActionModule {

    /**
     * 启动一个app
     * @param packageName 包名
     */
    fun launchApp(packageName: String)

    /**
     * 启动一个Activity页面
     * @param intent 意图
     * @param springboard 是否使用跳板转发 在后台启动第三方应用的时候会用到
     */
    fun startActivity(intent: Intent, springboard: Boolean = true)


    /**
     * 打开一个指定链接
     * @param url 链接地址
     * @param packageName 打开这个链接的包名
     * @return false代表没有能响应的程序
     */
    fun openUrl(url: String, packageName: String): Boolean

    /**
     * 跳转桌面
     */
    fun home()

    /**
     * 触发返回
     */
    fun back()

    /**
     * 电源长按弹窗
     */
    fun powerDialog()

    /**
     * 锁屏
     */
    @TargetApi(Build.VERSION_CODES.P)
    fun lockScreen()

    /**
     * 打开最近列表
     */
    fun recents()

    /**
     * 拉出通知栏
     */
    fun notifications()

    /**
     * 拉出通知的快速设置
     */
    fun quickSettings()

    /**
     * 分屏
     */
    fun toggleSplitScreen()

    /**
     * 截图
     */
    fun takeScreenshot()

    fun keycodeHeadsethook()

    fun dismissNotificationShade()

    fun accessibilityAllApps()


//    public static final int GLOBAL_ACTION_ACCESSIBILITY_BUTTON = 11;
//    public static final int GLOBAL_ACTION_ACCESSIBILITY_BUTTON_CHOOSER = 12;
//    public static final int GLOBAL_ACTION_ACCESSIBILITY_SHORTCUT = 13;
//    public static final int GLOBAL_ACTION_ACCESSIBILITY_ALL_APPS = 14;
}