package cn.wufou.auto.engine.modules.action.impl

import android.accessibilityservice.AccessibilityService
import android.annotation.TargetApi
import android.content.Intent
import android.net.Uri
import android.os.Build
import cn.wufou.auto.core.MagicCore
import cn.wufou.auto.engine.modules.action.ActionModule

class BasicActionModule : ActionModule {

    private val application = MagicCore.getApplication()
    private val magic = MagicCore.getMagicClient()


    override fun launchApp(packageName: String) {
        val intent = Intent()
        application.packageManager.getApplicationInfo(packageName, 0)
        application.startActivity(intent)
    }

    override fun startActivity(intent: Intent, springboard: Boolean) {
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        if (springboard) {
            HostActivity.startTargetActivity(application, intent)
        } else {
            application.startActivity(intent)
        }
    }

    override fun openUrl(url: String, packageName: String): Boolean {
        return try {
            val intent = Intent()
            intent.data = Uri.parse(url)
            intent.`package` = packageName
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            application.startActivity(intent)
            true
        } catch (e: Exception) {
            false
        }
    }

    override fun home() {
        magic.performGlobalAction(AccessibilityService.GLOBAL_ACTION_HOME)
    }

    override fun back() {
        magic.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK)
    }

    override fun powerDialog() {
        magic.performGlobalAction(AccessibilityService.GLOBAL_ACTION_POWER_DIALOG)
    }

    @TargetApi(Build.VERSION_CODES.P)
    override fun lockScreen() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            magic.performGlobalAction(AccessibilityService.GLOBAL_ACTION_LOCK_SCREEN)
        }
    }

    override fun recents() {
        magic.performGlobalAction(AccessibilityService.GLOBAL_ACTION_RECENTS)
    }

    override fun notifications() {
        magic.performGlobalAction(AccessibilityService.GLOBAL_ACTION_NOTIFICATIONS)
    }

    override fun quickSettings() {
        magic.performGlobalAction(AccessibilityService.GLOBAL_ACTION_QUICK_SETTINGS)
    }

    override fun toggleSplitScreen() {
        magic.performGlobalAction(AccessibilityService.GLOBAL_ACTION_TOGGLE_SPLIT_SCREEN)
    }

    override fun takeScreenshot() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            magic.performGlobalAction(AccessibilityService.GLOBAL_ACTION_TAKE_SCREENSHOT)
        }
    }

    override fun keycodeHeadsethook() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            magic.performGlobalAction(AccessibilityService.GLOBAL_ACTION_KEYCODE_HEADSETHOOK)
        }
    }

    override fun dismissNotificationShade() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            magic.performGlobalAction(AccessibilityService.GLOBAL_ACTION_DISMISS_NOTIFICATION_SHADE)
        }
    }

    override fun accessibilityAllApps() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            magic.performGlobalAction(AccessibilityService.GLOBAL_ACTION_ACCESSIBILITY_ALL_APPS)
        }
    }

}