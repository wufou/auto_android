package cn.wufou.auto.engine.modules.node.basic

import cn.wufou.auto.core.MagicCore
import cn.wufou.auto.engine.component.Component
import cn.wufou.auto.engine.component.ComponentFinder
import cn.wufou.auto.engine.modules.IModuleAdapter
import cn.wufou.auto.engine.modules.node.NodeModule
import cn.wufou.auto.engine.modules.node.entity.NodeInfo
import cn.wufou.auto.engine.modules.node.toNodeInfo
import kotlinx.coroutines.currentCoroutineContext
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive

/**
 * 基础版本的节点模块实现
 */
class BasicNodeModule : NodeModule {

    private val magic = MagicCore.getMagicClient()
    override fun getCurrentRootNodeInfo(): NodeInfo? {
        return magic.getRootNodeInfoInActiveWindow()?.toNodeInfo()
    }

    override suspend fun awaitCurrentRootNodeInfo(): NodeInfo {
        while (currentCoroutineContext().isActive) {
            val root = getCurrentRootNodeInfo()
            if (root != null) {
                return root
            }

        }
        throw RuntimeException("没有找到焦点窗口根节点")
    }

    override fun checkFeature(onlyVisibleToUser: Boolean, vararg featureFilter: (NodeInfo) -> Boolean): Boolean {
        return magic.getRootNodeInfoInActiveWindow()?.toNodeInfo()?.checkFeature(onlyVisibleToUser, *featureFilter) ?: false
    }


    override fun text(text: String): List<NodeInfo> {
        return magic.getRootNodeInfoInActiveWindow()?.findAccessibilityNodeInfosByText(text)?.map { it.toNodeInfo() } ?: emptyList()
    }

    override suspend fun await(timeout: Long, error: Throwable, block: (NodeInfo?) -> Boolean) {
        val startTime = System.currentTimeMillis()
        while (currentCoroutineContext().isActive) {
            if (System.currentTimeMillis() - startTime >= timeout) {
                throw error
            }
            val root = magic.getRootNodeInfoInActiveWindow()
            root?.refresh()
            if (block.invoke(root?.toNodeInfo())) {
                return
            }
            delay(300)
        }
        throw RuntimeException("任务已中断")
    }

    override suspend fun findOne(index: Int, onlyVisibleToUser: Boolean, filter: (NodeInfo) -> Boolean): NodeInfo? {
        return magic.getRootNodeInfoInActiveWindow()?.toNodeInfo()?.findOne(index, onlyVisibleToUser, filter)
    }

    override suspend fun <C : Component, F : ComponentFinder<C, NodeInfo>> findComponent(finder: Class<F>): C? {
        return magic.getRootNodeInfoInActiveWindow()?.toNodeInfo()?.findComponent(finder)
    }

    override suspend fun <C : Component, F : ComponentFinder<C, NodeInfo>> findComponentAwait(finder: Class<F>, timeout: Long, error: Throwable): C {
        val startTime = System.currentTimeMillis();
        while (currentCoroutineContext().isActive) {
            if (System.currentTimeMillis() - startTime >= timeout) {
                throw error
            }
            val component = findComponent(finder)
            if (component != null) {
                return component
            }
            delay(300)
        }
        throw RuntimeException("任务已中断")
    }

    override suspend fun findOneAwait(timeout: Long, error: Throwable, block: (NodeInfo?) -> NodeInfo?): NodeInfo {
        val startTime = System.currentTimeMillis()
        while (currentCoroutineContext().isActive) {
            if (System.currentTimeMillis() - startTime >= timeout) {
                throw error
            }
            val root = magic.getRootNodeInfoInActiveWindow()
            root?.refresh()
            val result = block.invoke(root?.toNodeInfo())
            if (result != null) {
                return result
            }
            delay(300)
        }
        throw RuntimeException("任务已中断")
    }


}