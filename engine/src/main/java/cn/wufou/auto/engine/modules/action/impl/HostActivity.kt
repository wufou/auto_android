package cn.wufou.auto.engine.modules.action.impl

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle

class HostActivity : Activity() {


    companion object {

        fun startTargetActivity(context: Context, intent: Intent) {
            val i = Intent(context, HostActivity::class.java)
            i.putExtra("target", intent)
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(i)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val target = intent?.getParcelableExtra<Intent>("target") ?: throw IllegalStateException("target无效")
        startActivity(target)
        finish()
    }

}