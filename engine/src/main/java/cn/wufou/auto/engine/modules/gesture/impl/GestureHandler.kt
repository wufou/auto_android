package cn.wufou.auto.engine.modules.gesture.impl

import android.accessibilityservice.AccessibilityService
import android.accessibilityservice.GestureDescription
import android.graphics.Path
import cn.wufou.auto.core.MagicCore
import cn.wufou.auto.engine.modules.gesture.entity.GestureDescription as GD
import kotlinx.coroutines.delay
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

internal object GestureHandler {


    private val client = MagicCore.getMagicClient()


    /**
     * 执行滑动手势
     */
    suspend fun gesture( points: List<GD.GesturePoint>): Boolean {
//        val lines = points.mapIndexed { index, point ->
//            if (index < points.size - 1) {
//                val nextPoint = points[index + 1]
//                point to nextPoint
//            } else {
//                null
//            }
//        }.filterNotNull()
//        var sd: GestureDescription.StrokeDescription? = null
//        for (index in 0..lines.size - 2) {
//            val from = points[index]
//            val to = points[index + 1]
//            sd = moveGesture(from.x, from.y, to.x, to.y, 100, sd, index < lines.size - 2)
//            if (sd == null) {
//                return false
//            }
//        }
//        return true
        val lines = points.mapIndexed { index, point ->
            if (index < points.size - 1) {
                val nextPoint = points[index + 1]
                point to nextPoint
            } else {
                null
            }
        }.filterNotNull()
        var sd: GestureDescription.StrokeDescription? = null
        for (index in lines.indices) {
            val from = lines[index].first
            val to = lines[index].second
            sd = moveGesture(from.x, from.y, to.x, to.y, 100, sd, index < lines.size-1)
            if (sd == null) {
                return false
            }
        }
        return true
    }


    private var time = 0L

    private suspend fun moveGesture(
        fx: Float,
        fy: Float,
        tx: Float,
        ty: Float,
        duration: Long,
        source: GestureDescription.StrokeDescription?,
        willContinue: Boolean
    ): GestureDescription.StrokeDescription? {
        println("消耗时间${System.currentTimeMillis() - time}")
        time = System.currentTimeMillis()
        if (fx == tx && ty == fy) {
            // 位置不变直接延时
            delay(duration)
//            return source
        }
        val gs = GestureDescription.Builder()
        val path = Path()
        path.moveTo(fx, fy)
        path.lineTo(tx, ty)
        val gd = if (source != null) {
            source.continueStroke(path, 0, duration, willContinue)
        } else {
            GestureDescription.StrokeDescription(path, 0, duration, willContinue)
        }
        gs.addStroke(gd)
        return suspendCoroutine {
            val result = client.dispatchGestureEvent(gs.build(), object : AccessibilityService.GestureResultCallback() {
                override fun onCompleted(gestureDescription: GestureDescription) {
                    it.resume(gd)
                }

                override fun onCancelled(gestureDescription: GestureDescription?) {
                    it.resume(null)
                }
            }, null)
            if (!result) {
                it.resume(null)
            }
        }

    }

}