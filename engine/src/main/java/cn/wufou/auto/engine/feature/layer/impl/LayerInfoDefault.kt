package cn.wufou.auto.engine.feature.layer.impl

import android.graphics.Rect
import androidx.core.view.accessibility.AccessibilityNodeInfoCompat
import cn.wufou.auto.engine.feature.layer.LayerInfo

object LayerInfoDefault : LayerInfo {

    override val rect: Rect = Rect()
    override var order: Int = 0
    override val children: List<LayerInfo> = emptyList()

    override fun refresh() {
    }


}