package cn.wufou.auto.engine.modules.toast

interface ToastModule {

    fun show(content:CharSequence)

}