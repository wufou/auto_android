package cn.wufou.auto.engine.modules.gesture

import android.accessibilityservice.GestureDescription

class Constant {
    companion object {
        /**
         *
         */
        val GESTURE_DURATION_MAX = GestureDescription.getMaxGestureDuration()
    }
}
