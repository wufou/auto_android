package cn.wufou.auto.engine.modules.window.entity

import android.view.accessibility.AccessibilityWindowInfo
import androidx.core.view.accessibility.AccessibilityWindowInfoCompat
import cn.wufou.auto.engine.modules.node.entity.NodeInfo
import cn.wufou.auto.engine.modules.node.toNodeInfo

/**
 * 窗口信息
 */
class WindowInfo(private val info: AccessibilityWindowInfoCompat) {

    fun getTitle(): CharSequence? {
        return info.title
    }

    fun getRootNodeInfo(): NodeInfo? {
        return info.root?.toNodeInfo()
    }

    override fun toString(): String {
        return "WindowInfo(info=$info)"
    }


}