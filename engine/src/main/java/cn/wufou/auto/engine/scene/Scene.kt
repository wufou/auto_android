package cn.wufou.auto.engine.scene

import cn.wufou.auto.engine.modules.EngineContext

/**
 * 定义场景的执行内容
 */
interface Scene {

    /**
     * 执行内容
     * @return true 执行成功 相当于break; 否则继续在场景中执行
     */
    suspend fun execute(context: EngineContext): Boolean
}