package cn.wufou.auto.engine.modules.toast.basic

import android.os.Handler
import android.os.Looper
import android.widget.Toast
import cn.wufou.auto.core.MagicCore
import cn.wufou.auto.engine.modules.toast.ToastModule

class BasicToastModule : ToastModule {
    private val mainHandler = Handler(Looper.getMainLooper())
    override fun show(content: CharSequence) {
        mainHandler.post {
            val application = MagicCore.getApplication()
            Toast.makeText(application, content, Toast.LENGTH_SHORT).show()
        }
    }
}