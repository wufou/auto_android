package cn.wufou.auto.engine.scene

import cn.wufou.auto.engine.modules.EngineContext

/**
 * 定义场景的触发条件和执行内容
 * 在某些自动化场景中可能会有不同执行操作分支，可以根据页面条件来执行树状的脚本链（本质就是if-else只是可读性更高）
 */
interface ConditionScene :Scene{
    /**
     * 执行条件
     *  @return true 符合条件
     */
    suspend fun condition(context: EngineContext): Boolean

}