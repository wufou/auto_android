package cn.wufou.auto.engine.modules.gesture

import cn.wufou.auto.engine.modules.gesture.entity.GestureDescription

interface GestureModule {

    /**
     * 执行一个手势
     */
    suspend fun dispatchGesture(gestureDescription: GestureDescription)


}