package cn.wufou.auto.engine.modules.node.entity

import android.graphics.Path
import android.graphics.Rect
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.accessibility.AccessibilityNodeInfo
import androidx.core.os.bundleOf
import androidx.core.view.accessibility.AccessibilityNodeInfoCompat
import cn.wufou.auto.core.MagicCore
import cn.wufou.auto.core.magic.IMagicClient
import cn.wufou.auto.engine.AutoEngine
import cn.wufou.auto.engine.component.Component
import cn.wufou.auto.engine.component.ComponentFinder
import cn.wufou.auto.engine.modules.gesture.entity.GestureDescription
import cn.wufou.auto.engine.modules.node.toNodeInfo
import kotlinx.coroutines.*


/**
 * 封装了一个节点信息
 */
class NodeInfo private constructor(private val source: AccessibilityNodeInfoCompat) {


    companion object {
        fun from(accessibilityNodeInfo: AccessibilityNodeInfo): NodeInfo {
            MagicCore.getMagicClient()
            return from(AccessibilityNodeInfoCompat.wrap(accessibilityNodeInfo))
        }

        fun from(accessibilityNodeInfo: AccessibilityNodeInfoCompat): NodeInfo {
            return NodeInfo(accessibilityNodeInfo)
        }
    }

    private val client: IMagicClient = MagicCore.getMagicClient()

    fun getParentNodeInfo(): NodeInfo? {
        val parent = source.parent
        return if (parent != null) {
            NodeInfo(parent)
        } else {
            null
        }
    }

    fun getRootNodeInfo(): NodeInfo? {
        val parent = source.window.root
        return if (parent != null) {
            NodeInfo(parent)
        } else {
            null
        }
    }

    fun getChildNodeInfo(index: Int): NodeInfo? {
        try {
            val child = source.getChild(index)
            if (child != null) {
                return NodeInfo(child)
            } else {
                return null
            }
        } catch (e: Exception) {
            e.printStackTrace()
            return null
        }
    }

    fun getChildCount(): Int {
        return source.childCount
    }

    fun getChildNodeInfoList(): List<NodeInfo> {
        val list = mutableListOf<NodeInfo>()
        for (index in 0 until source.childCount) {
            try {
                val child = source.getChild(index)
                if (child != null) {
                    list.add(NodeInfo(child))
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        return list
    }

    /**
     * 获取所属包名
     */
    fun getPackageName(): CharSequence? {
        return source.packageName
    }

    /**
     * 视图id
     */
    fun getViewIdResourceName(): String? {
        return source.viewIdResourceName
    }

    fun getText(): CharSequence? {
        return source.text
    }

    fun getTextString(): String? {
        return source.text?.toString()
    }

    fun getContentDesc(): CharSequence? {
        return source.contentDescription
    }

    fun getDescString(): String? {
        return source.contentDescription?.toString()
    }


    fun performClick(): Boolean {
        return source.performAction(AccessibilityNodeInfo.ACTION_CLICK)
    }

    fun focus(): Boolean {
        return source.performAction(AccessibilityNodeInfo.ACTION_FOCUS)
    }

    fun clearFocus(): Boolean {
        return source.performAction(AccessibilityNodeInfo.ACTION_CLEAR_FOCUS)
    }


    fun scroll(): Boolean {
        return source.performAction(AccessibilityNodeInfo.ACTION_SCROLL_FORWARD)
    }

    suspend fun scrollTop() {
        if (this.getSource().isScrollable) {
            var canScroll = true
            while (canScroll) {
                canScroll = this.getSource().performAction(AccessibilityNodeInfo.ACTION_SCROLL_BACKWARD)
                delay(100)
            }
        }
    }


    private fun performLongClick(): Boolean {
        return source.performAction(AccessibilityNodeInfo.ACTION_LONG_CLICK)
    }

    fun getSource(): AccessibilityNodeInfoCompat {
        return source
    }

    fun isVisibleToUser(): Boolean {
        return source.isVisibleToUser
    }

    fun isClickable(): Boolean {
        return source.isClickable
    }

    fun isSelected(): Boolean {
        return source.isSelected
    }

    fun isChecked(): Boolean {
        return source.isChecked
    }

    fun isEditable(): Boolean {
        return source.isEditable
    }

    fun isEnabled(): Boolean {
        return source.isEnabled
    }

    fun isLongClickable(): Boolean {
        return source.isLongClickable
    }

    fun isScrollable(): Boolean {
        return source.isScrollable
    }

    fun isCheckable(): Boolean {
        return source.isCheckable
    }

    /**
     * 获取绘制的排序
     */
    fun getDrawingOrder(): Int {
        return source.drawingOrder
    }

    fun getClassName(): CharSequence {
        return source.className
    }

    /**
     * 刷新节点信息
     */
    fun refresh(): Boolean {
        return source.refresh()
    }


    /**
     * 设置文本
     */
    fun setText(text: String): Boolean {
        return source.performAction(
            AccessibilityNodeInfoCompat.ACTION_SET_TEXT, bundleOf(
                AccessibilityNodeInfoCompat.ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE to text
            )
        )
    }

    /**
     * 按压屏幕
     */
    suspend fun tap() {
        val rect = Rect()
        getSource().getBoundsInScreen(rect)
//        val path = Path()
//        path.moveTo(rect.exactCenterX(), rect.exactCenterY())
//        path.lineTo(rect.exactCenterX(), rect.exactCenterY())
//        val gd = GestureDescription.Builder()
//            .addStroke(GestureDescription.StrokeDescription(path, 0, 100))
//            .build()
        val gd = GestureDescription.Builder()
            .moveTo(rect.exactCenterX(), rect.exactCenterY())
            .lineTo(rect.exactCenterX(), rect.exactCenterY())
            .build()
        AutoEngine.getModuleAdapter().gesture.dispatchGesture(gd)
    }

    fun showOnScreen() {
        source.actionList.find {
            it == AccessibilityNodeInfoCompat.AccessibilityActionCompat.ACTION_SHOW_ON_SCREEN
        }?.apply {
            source.performAction(this.id)
        }
    }

    /**
     * 单击
     */
    fun click(up: Boolean = false): Boolean {
        when {
            this.getSource().isClickable -> {
                // 自己能点击
                return this.performClick()
            }
            up -> {
                // 向上处理
                var parent = this.getParentNodeInfo()
                while (parent != null) {
                    if (parent.getSource().isClickable) {
                        parent.performClick()
                        return true
                    }
                    parent = parent.getParentNodeInfo()
                }
                return false
            }
            else -> {
                return false
            }
        }
    }

    /**
     * 单击
     */
    fun longClick(up: Boolean = false): Boolean {
        when {
            this.getSource().isLongClickable -> {
                // 自己能点击
                return this.performLongClick()
            }
            up -> {
                // 向上处理
                var parent = this.getParentNodeInfo()
                while (parent != null) {
                    if (parent.getSource().isLongClickable) {
                        parent.performLongClick()
                        return true
                    }
                    parent = parent.getParentNodeInfo()
                }
                return false
            }
            else -> {
                return false
            }
        }
    }

    /**
     * 找到最新的符合条件的父节点
     */
    fun findParent(callback: (nodeInfo: NodeInfo) -> Boolean): NodeInfo? {
        var parent = getParentNodeInfo()
        while (parent != null) {
            if (callback.invoke(parent)) {
                return parent
            }
            parent = parent.getParentNodeInfo()
        }
        return null
    }

    fun findParentEqualsText(text: String): NodeInfo? {
        return findParent {
            it.getTextString() == text
        }
    }

    fun findParentEqualsDesc(text: String): NodeInfo? {
        return findParent {
            it.getDescString() == text
        }
    }

    /**
     * 按文本找到节点
     */
    fun findListByText(text: String): List<NodeInfo> {
        return source.findAccessibilityNodeInfosByText(text)?.map { it.toNodeInfo() } ?: emptyList()
    }

    enum class FindTextMode {
        ALL,
        TEXT,
        DESC
    }

    enum class FindTextMatch {
        EQ,
        CONTAINS,
    }

    /**
     * 按文本找到节点
     */
    fun findOneByText(text: String, mode: FindTextMode = FindTextMode.ALL, match: FindTextMatch = FindTextMatch.CONTAINS): NodeInfo? {
        return findOne { node ->
            when (mode) {
                FindTextMode.ALL -> {
                    if (match == FindTextMatch.CONTAINS) {
                        node.getTextString()?.contains(text) ?: false || node.getDescString()?.contains(text) ?: false
                    } else {
                        node.getTextString() == text || node.getDescString() == text
                    }
                }
                FindTextMode.TEXT -> {
                    if (match == FindTextMatch.CONTAINS) {
                        node.getTextString()?.contains(text) ?: false
                    } else {
                        node.getTextString() == text
                    }
                }
                FindTextMode.DESC -> {
                    if (match == FindTextMatch.CONTAINS) {
                        node.getDescString()?.contains(text) ?: false
                    } else {
                        node.getDescString() == text
                    }
                }
            }
        }
//        return source.findAccessibilityNodeInfosByText(text)?.firstOrNull()?.toNodeInfo()
    }

    /**
     * 找到指定顺序符合条件的节点
     * @param index 节点序号 第几个符合条件的节点，在某些有多个类似元素的场景下可能会用到
     * @param onlyVisibleToUser 是否是用户可见的节点
     * @param filter 过滤函数
     */
    fun findOne(index: Int = 0, onlyVisibleToUser: Boolean = true, filter: (NodeInfo) -> Boolean): NodeInfo? {
        var count = 0 //计数器
        fun find(node: NodeInfo): NodeInfo? {
            if (node.refresh()) {
                if (onlyVisibleToUser && !node.isVisibleToUser()) {
                    // 不可见元素 忽略
                    return null
                }
                if (filter.invoke(node)) {
                    if (count == index) {
                        return node
                    } else {
                        count++
                    }
                }
                val childList = node.getChildNodeInfoList()
                for (item in childList) {
                    val childRes = find(item)
                    if (childRes != null) {
                        return childRes
                    }
                }
            }
            return null
        }
        return find(this)
    }

    /**
     * 等待查询
     */
    suspend fun findOneAwait(timeout: Long = 5000, error: Throwable, block: (NodeInfo?) -> NodeInfo?): NodeInfo {
        val startTime = System.currentTimeMillis()
        while (currentCoroutineContext().isActive) {
            if (System.currentTimeMillis() - startTime >= timeout) {
                throw error
            }
            refresh()
            val result = block.invoke(this)
            if (result != null) {
                return result
            }
            delay(300)
        }
        throw RuntimeException("任务已中断")
    }


    /**
     * 检查节点特征
     */
    fun checkFeature(onlyVisibleToUser: Boolean = true, vararg featureFilter: (NodeInfo) -> Boolean): Boolean {
        val filterList = featureFilter.toMutableList()
        if (featureFilter.isEmpty()) {
            // 空条件
            return false
        }
        val wrapFilter: (NodeInfo) -> Boolean = { node ->
            filterList.any { filter ->
                val res = filter.invoke(node)
                if (res) {
                    filterList.remove(filter)
                }
                res
            }
            false
        }
        filterNodeInfo(onlyVisibleToUser, wrapFilter)
        return filterList.isEmpty()
    }

    /**
     * 遍历节点下所有符合条件的节点
     * @param onlyVisibleToUser 是否是用户可见的节点
     * @param filter 过滤函数
     */
    fun filterNodeInfo(
        onlyVisibleToUser: Boolean = true,
        filter: (NodeInfo) -> Boolean,
    ): List<NodeInfo> {
        val res = mutableListOf<NodeInfo>()
        if (!this.refresh()) {
            return res
        }
        if (onlyVisibleToUser && !this.isVisibleToUser()) {
            return res
        }
        if (filter.invoke(this)) {
            res.add(this)
        }
        val childList = this.getChildNodeInfoList()
        for (item in childList) {
            val ni = item.filterNodeInfo(onlyVisibleToUser, filter)
            if (ni.isNotEmpty()) {
                res.addAll(ni)
            }
        }
        return res
    }


    /**
     * 输入
     */
    fun input(
        text: String,
        index: Int = 0,
        onlyVisibleToUser: Boolean = true,
        filter: ((NodeInfo) -> Boolean)? = null,
    ): Boolean {
        // 找到页面上的所有可输入元素
        val nodeInfo = this.findOne(index, onlyVisibleToUser) {
            it.getSource().className == "android.widget.EditText" && (filter?.invoke(it) ?: true)
        }
        return nodeInfo?.getSource()?.performAction(
            AccessibilityNodeInfoCompat.ACTION_SET_TEXT, bundleOf(
                AccessibilityNodeInfoCompat.ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE to text
            )
        ) ?: false
    }


    suspend fun findOneFromScrollableView(onlyVisibleToUser: Boolean = true, interval: Long = 100, filter: (NodeInfo) -> Boolean): NodeInfo? {
        do {
            val node = findOne(onlyVisibleToUser = onlyVisibleToUser, filter = filter)
            if (node != null) {
                return node
            }
            delay(interval)
        } while (scroll())
        return null
    }


    suspend fun <C : Component, F : ComponentFinder<C, NodeInfo>> findComponent(finder: Class<F>): C? {
        return ComponentFinder.find(finder, this)
    }


    suspend fun <C : Component, F : ComponentFinder<C, NodeInfo>> findComponentAwait(finder: Class<F>, timeout: Long, error: Throwable): C {
        val startTime = System.currentTimeMillis();
        while (currentCoroutineContext().isActive) {
            if (System.currentTimeMillis() - startTime >= timeout) {
                throw error
            }
            val component = findComponent(finder)
            if (component != null) {
                return component
            }
            delay(300)
        }
        throw RuntimeException("任务已中断")
    }


    /**
     * 打印节点信息
     */
    fun printSimpleNodeInfo() {
        printSimpleNodeInfo(this, 0)
    }

    private fun printSimpleNodeInfo(nodeInfo: NodeInfo, layerCount: Int) {
        val placeholder = Array(layerCount) { "--" }.joinToString("")
        Log.i("NodeInfo_Data", "$placeholder class:【${nodeInfo.getClassName()}】,text:${nodeInfo.getTextString()},desc:${nodeInfo.getDescString()}")
        val list = nodeInfo.getChildNodeInfoList()
        for (item in list) {
            printSimpleNodeInfo(item, layerCount + 1)
        }
    }

    /**
     * 查找RecyclerView下面item符合条件的第一个节点 只查询直接子节点
     */
    fun findRecyclerViewItem(callback: (itemNode: NodeInfo) -> Boolean): NodeInfo? {
        do {
            val item = getChildNodeInfoList().find(callback)
            if (item != null) {
                return item
            }
        } while (scroll())
        return null
    }

    /**
     * 等待节点符合条件
     */
    suspend fun await(timeout: Long = 5000, error: Throwable, block: (NodeInfo?) -> Boolean) {
        try {
            withTimeout(timeout) {
                while (currentCoroutineContext().isActive) {
                    refresh()
                    val result = block.invoke(this@NodeInfo)
                    if (result) {
                        return@withTimeout
                    }
                    delay(300)
                }
            }
        } catch (e: TimeoutCancellationException) {
            throw error
        }
        throw RuntimeException("任务已中断")
    }


}