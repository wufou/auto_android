package cn.wufou.auto.engine.component

import cn.wufou.auto.engine.AutoEngine
import cn.wufou.auto.engine.modules.node.entity.NodeInfo

abstract class Component(source: NodeInfo) {
    var root = source
        private set

    /**
     * 刷新
     */
    suspend fun <C : Component, F : ComponentFinder<C, NodeInfo>> refresh(finder: Class<F>, timeout: Long, error: Exception) {
        val root = AutoEngine.geEngineContext().node.findComponentAwait(finder, timeout, error).root
        this.root = root
    }


    /**
     * 继续查找子组件
     */
    suspend fun <C : Component, T : ComponentFinder<C, NodeInfo>> find(finder: Class<T>): C? {
        return root.findComponent(finder)
    }

    /**
     * 继续查找子组件
     */
    suspend fun <C : Component, T : ComponentFinder<C, NodeInfo>> findAwait(finder: Class<T>, timeout: Long, error: Throwable): C {
        return root.findComponentAwait(finder, timeout, error)
    }

}