package cn.wufou.auto.engine.feature.layer

import androidx.core.view.accessibility.AccessibilityNodeInfoCompat
import cn.wufou.auto.engine.feature.layer.impl.LayerInfoDefault
import cn.wufou.auto.engine.feature.layer.impl.LayerInfoImpl

class LayerInfoBuilder(private val source: AccessibilityNodeInfoCompat?) {

    fun build(): LayerInfo {
        return source?.run { LayerInfoImpl(this) } ?: LayerInfoDefault
    }
}