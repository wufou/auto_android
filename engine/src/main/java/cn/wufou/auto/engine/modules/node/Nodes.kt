package cn.wufou.auto.engine.modules.node

import android.view.accessibility.AccessibilityNodeInfo
import androidx.core.view.accessibility.AccessibilityNodeInfoCompat
import cn.wufou.auto.engine.feature.layer.LayerInfo
import cn.wufou.auto.engine.feature.layer.LayerInfoBuilder
import cn.wufou.auto.engine.modules.node.basic.WindowNodeModule
import cn.wufou.auto.engine.modules.node.entity.NodeInfo
import cn.wufou.auto.engine.modules.window.entity.WindowInfo

fun AccessibilityNodeInfo.toNodeInfo(): NodeInfo {
    return NodeInfo.from(this)
}

fun AccessibilityNodeInfoCompat.toNodeInfo(): NodeInfo {
    return NodeInfo.from(this)
}


fun NodeInfo.getLayerInfo(): LayerInfo {
    return LayerInfoBuilder(this.getSource()).build()
}
