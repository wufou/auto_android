package cn.wufou.auto.engine.modules.gesture.impl

import android.accessibilityservice.AccessibilityService
import android.graphics.Path
import cn.wufou.auto.core.MagicCore
import cn.wufou.auto.engine.modules.gesture.GestureModule
import cn.wufou.auto.engine.modules.gesture.entity.GestureDescription

/**
 * 基础实现
 */
class BasicGestureModule : GestureModule {


    override suspend fun dispatchGesture(gestureDescription: GestureDescription) {
        val points = gestureDescription.getPoints()
        GestureHandler.gesture(points)
        // 手势执行需要两重保底 1是手势执行超时 2是手势执行完成/取消
    }


}