package cn.wufou.auto.engine.modules.window

import android.view.accessibility.AccessibilityWindowInfo
import androidx.core.view.accessibility.AccessibilityWindowInfoCompat
import cn.wufou.auto.engine.feature.layer.LayerInfo
import cn.wufou.auto.engine.feature.layer.LayerInfoBuilder
import cn.wufou.auto.engine.modules.node.NodeModule
import cn.wufou.auto.engine.modules.node.basic.WindowNodeModule
import cn.wufou.auto.engine.modules.node.toNodeInfo
import cn.wufou.auto.engine.modules.window.entity.WindowInfo


fun AccessibilityWindowInfo.toWindowInfo(): WindowInfo {
    return this.root.toNodeInfo().getSource().window.toWindowInfo()
}

fun AccessibilityWindowInfoCompat.toWindowInfo(): WindowInfo {
    return WindowInfo(this)
}

fun WindowInfo.toWindowNode(): NodeModule {
    return WindowNodeModule(this)
}


fun WindowInfo.getLayerInfo(): LayerInfo {
    return LayerInfoBuilder(this.getRootNodeInfo()?.getSource()).build()
}
