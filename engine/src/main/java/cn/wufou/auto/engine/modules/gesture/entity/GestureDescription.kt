package cn.wufou.auto.engine.modules.gesture.entity


/**
 * 完全复原手势是一个十分困难的事，由于手势时按path的length按持续时间去计算出指定时间的进度的，因此一旦手势前后的华东速度不同，会导致这个速度被抹掉计算成一个均值,
 * 因此想要还原这个力度就需要拆分路径，有力的短路径，轻柔的长路径
 */
class GestureDescription() {

    companion object {
        /**
         * 可供参考的默认持续时间
         */
        const val DEFAULT_DURATION: Long = 100
    }

    class Builder {

        private val gd = GestureDescription()


        fun moveTo(x: Float, y: Float): Builder {
            gd.moveTo(x, y)
            return this
        }

        fun lineTo(x: Float, y: Float, duration: Long = DEFAULT_DURATION): Builder {
            gd.lineTo(x, y, duration)
            return this
        }

        fun build(): GestureDescription {
            return GestureDescription().apply {
                this.points.addAll(gd.points)
            }
        }
    }

    /**
     * @param x x坐标
     * @param y y坐标
     * @param duration 移动点位的持续时间
     * @param willContinue 是否从上一个点位移动过来
     */
    data class GesturePoint(val x: Float, val y: Float, val duration: Long, val willContinue: Boolean)

    // 记录所有手势点位
    private val points = mutableListOf<GesturePoint>()

    /**
     * 移动点位
     */
    private fun moveTo(x: Float, y: Float) {
        points.add(GesturePoint(x, y, 0, false))
    }

    /**
     * 移动路径
     */
    private fun lineTo(x: Float, y: Float, duration: Long = DEFAULT_DURATION) {
        if (this.points.isEmpty()) {
            // 没有起始点位咱就自己干,持续时间什么的滚蛋
            this.moveTo(x, y)
            return
        }
        points.add(GesturePoint(x, y, duration, true))

    }

    /**
     * 获取手势点
     */
    fun getPoints(): List<GesturePoint> {
        return ArrayList(points)
    }


}