package cn.wufou.auto.bot.test

import cn.wufou.auto.bot.Bot
import cn.wufou.auto.bot.context.BotContext
import cn.wufou.auto.engine.modules.window.getLayerInfo
import kotlinx.coroutines.delay

internal class TestBot : Bot {

    override suspend fun execute(context: BotContext) {
        context.apply {
            toast.show("嗨，欢迎使用测试Bot，即将在三秒后退回桌面")
            delay(3000)
            this.action.home()
            toast.show("Nice！操作成功了！")
        }
    }

}