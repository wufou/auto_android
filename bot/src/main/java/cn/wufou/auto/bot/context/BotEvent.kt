package cn.wufou.auto.bot.context

data class BotEvent(val tag: String, val data: Any?)