package cn.wufou.auto.bot

import java.io.Serializable

/**
 *  定义一个创建任务执行脚本的工厂类
 */
interface BotBox : Serializable {


    /**
     * 创建可执行脚本
     */
    fun read(): Bot


    /**
     * 创建新的子脚本,每个子脚本单独运行在一个工作线程
     */
    fun createSubBot(): List<Bot> {
        return emptyList()
    }


}