package cn.wufou.auto.bot.context

import android.app.Application
import cn.wufou.auto.engine.modules.IModuleAdapter

/**
 * Bot上下文环境
 */
interface BotContext : IModuleAdapter {

    /**
     * 获取应用上下文
     */
    fun getApplication(): Application


    /**
     * 派遣事件
     */
    fun dispatchEvent(name: String, data: Any? = null)

    /**
     * 注册一个事件回调
     */
    fun registerEventCallback(eventTags: List<String>, callback: (BotEvent) -> Boolean): (BotEvent) -> Boolean

    /**
     * 注销一个事件回调
     */
    fun unregisterEventCallback(callback: (BotEvent) -> Boolean)

}