package cn.wufou.auto.bot.test

import cn.wufou.auto.bot.Bot
import cn.wufou.auto.bot.BotBox

internal class TestBotBox : BotBox {
    override fun read(): Bot {
        return TestBot()
    }
}