package cn.wufou.auto.bot

import android.content.Intent
import cn.wufou.auto.bot.exception.BotNotFoundException
import cn.wufou.auto.bot.service.BotService
import cn.wufou.auto.bot.test.TestBotBox
import cn.wufou.auto.engine.AutoEngine

/**
 * 自动化脚本客户端
 */
object BotClient {

    private var botBox: BotBox? = null

    /**
     * 装载一个Bot工厂
     */
    fun mount(botBox: BotBox) {
        this.botBox = botBox
    }

    fun open() {
        AutoEngine.start()
    }

    fun close() {
        AutoEngine.stop()
    }

    fun envConfig(): BotEnvConfig {
        return BotEnvConfig
    }


    /**
     * 快捷启动Bot
     */
    fun launch(botBox: BotBox,callback: (() -> Unit)? = null) {
        this.mount(botBox)
        this.launch(callback)
    }

    /**
     * 启动Bot
     */
    fun launch(callback: (() -> Unit)? = null) {
        if (!AutoEngine.isEngineEnable()) {
            AutoEngine.start {
                callback?.invoke()
                val context = AutoEngine.getApplication()
                val botFactory = this.botBox ?: throw  BotNotFoundException("Not Mount BotFactory")
                val intent = Intent(context, BotService::class.java)
                intent.putExtra(BotService.PARAM_CMD_FLAG, BotService.CMD_START)
                intent.putExtra(BotService.PARAM_BOT_FACTORY, botFactory)
                context.startService(intent)
            }
            return
        }
        val context = AutoEngine.getApplication()
        val botFactory = this.botBox ?: throw  BotNotFoundException("Not Mount BotFactory")
        val intent = Intent(context, BotService::class.java)
        intent.putExtra(BotService.PARAM_CMD_FLAG, BotService.CMD_START)
        intent.putExtra(BotService.PARAM_BOT_FACTORY, botFactory)
        context.startService(intent)
    }

    /**
     * 停止bot
     */
    fun stop() {
        if (!AutoEngine.isEngineEnable()) {
            return
        }
        val context = AutoEngine.getApplication()
        val intent = Intent(context, BotService::class.java)
        intent.putExtra(BotService.PARAM_CMD_FLAG, BotService.CMD_STOP)
        context.startService(intent)
    }

    /**
     * 启动测试
     */
    fun test() {
        if (!AutoEngine.isEngineEnable()) {
            AutoEngine.start()
            return
        }
        val context = AutoEngine.getApplication()
        val intent = Intent(context, BotService::class.java)
        intent.putExtra(BotService.PARAM_BOT_FACTORY, TestBotBox())
        context.startService(intent)
    }

}