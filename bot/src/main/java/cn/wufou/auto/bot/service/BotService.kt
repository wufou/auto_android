package cn.wufou.auto.bot.service

import android.content.Intent
import android.os.IBinder
import androidx.lifecycle.LifecycleService
import androidx.lifecycle.lifecycleScope
import cn.wufou.auto.bot.BotBox
import cn.wufou.auto.bot.context.BotContextImpl
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.concurrent.CancellationException
import kotlin.concurrent.thread

/**
 * 脚本服务
 */
internal class BotService : LifecycleService() {

    companion object {
        const val PARAM_BOT_FACTORY = "param_boy_factory"
        const val PARAM_CMD_FLAG = "param_cmd_flag"


        const val CMD_START = "cmd_start"
        const val CMD_STOP = "cmd_stop"
    }

    private val binder = BotBinder()
    private var lastJob: Job? = null


    override fun onCreate() {
        super.onCreate()
    }

    override fun onBind(intent: Intent): IBinder? {
        super.onBind(intent)
        return binder
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)
        if (flags == 0) {
            // 客户端发出的启动命令
            val cmd = intent?.getStringExtra(PARAM_CMD_FLAG)
            when (cmd) {
                CMD_START -> {
                    val factory = intent.getSerializableExtra(PARAM_BOT_FACTORY)
                    if (factory != null && factory is BotBox) {
                        // 收到新的命令
                        startBot(factory)
                    }
                }
                CMD_STOP -> {
                    stopBot()
                }
            }
        }
        return START_NOT_STICKY
    }

    /**
     * 自动自动化程序
     */
    private fun startBot(box: BotBox) {
        this.lastJob?.cancel(CancellationException("新的任务已启动，当前任务取消"))
        val job = lifecycleScope.launchWhenCreated {
            val botContext = BotContextImpl(application)
            try {
                val bot = box.read()
                // 启动工作线程
                val subBotList = box.createSubBot()
                subBotList.forEach { subBot->
                    launch {
                        withContext(Dispatchers.IO){
                            subBot.execute(botContext)
                        }
                    }
                }
                // 执行主Bot
                bot.execute(botContext)
            }finally {
            }
        }
        this.lastJob = job
    }

    private fun stopBot() {
        this.lastJob?.cancel(CancellationException("任务取消"))
    }

}