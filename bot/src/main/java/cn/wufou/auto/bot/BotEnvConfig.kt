package cn.wufou.auto.bot

import cn.wufou.auto.engine.AutoEngine

object BotEnvConfig {

    fun setNotifyTitle(title: CharSequence) {
        AutoEngine.setForegroundNotifyTitle(title)
    }

    fun setNotifyText(text: CharSequence) {
        AutoEngine.setForegroundNotifyText(text)
    }

}