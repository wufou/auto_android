package cn.wufou.auto.bot.context

import android.app.Application

internal class BotContextImpl(private val application: Application) : EngineModuleBotContext() {

    private val callbackEventTags: LinkedHashMap<(BotEvent) -> Boolean, List<String>> = LinkedHashMap()


    override fun getApplication(): Application {
        return application
    }

    override fun dispatchEvent(name: String, data: Any? ) {
        val event = BotEvent(name,data)
        val callbacks = callbackEventTags.filterValues {
            it.contains(name)
        }
        for (callback in callbacks) {
            if (callback.key.invoke(event)) {
                return
            }
        }
    }

    override fun registerEventCallback(eventTags: List<String>, callback: (BotEvent) -> Boolean): (BotEvent) -> Boolean {
        callbackEventTags[callback] = eventTags
        return callback
    }

    override fun unregisterEventCallback(callback: (BotEvent) -> Boolean) {
        callbackEventTags.remove(callback)
    }
}