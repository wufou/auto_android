package cn.wufou.auto.bot.context

import cn.wufou.auto.engine.AutoEngine
import cn.wufou.auto.engine.modules.IModuleAdapter

/**
 * Bot上下文环境
 */
abstract class EngineModuleBotContext() : BotContext, IModuleAdapter by AutoEngine.getModuleAdapter() {


}