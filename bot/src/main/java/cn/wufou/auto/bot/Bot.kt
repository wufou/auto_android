package cn.wufou.auto.bot

import cn.wufou.auto.bot.context.BotContext

/**
 * 可完成任务的自动脚本
 */
interface Bot {

    /**
     * 执行自动化脚本
     */
    suspend fun execute(context: BotContext)


}