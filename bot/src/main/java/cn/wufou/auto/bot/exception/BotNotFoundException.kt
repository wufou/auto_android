package cn.wufou.auto.bot.exception

class BotNotFoundException(message: String = "Not Found Bot") : RuntimeException(message) {
}