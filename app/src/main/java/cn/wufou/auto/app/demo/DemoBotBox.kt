package cn.wufou.auto.app.demo

import cn.wufou.auto.bot.Bot
import cn.wufou.auto.bot.BotBox
import cn.wufou.auto.bot.context.BotContext
import cn.wufou.auto.engine.modules.gesture.entity.GestureDescription
import cn.wufou.auto.engine.modules.window.getLayerInfo

class DemoBotBox : BotBox {
    override fun read(): Bot {
        return object : Bot {
            override suspend fun execute(context: BotContext) {
                context.apply {
                    val gd = GestureDescription.Builder()
                        .moveTo(300f,300f)
                        .lineTo(310f,310f)
                        .lineTo(340f,340f)
                        .lineTo(350f,360f)
                        .lineTo(360f,410f)
                        .lineTo(360f,410f)
                        .lineTo(360f,410f)
                        .lineTo(360f,410f)
                        .lineTo(360f,410f)
                        .lineTo(360f,410f)
                        .lineTo(360f,410f)
                        .lineTo(360f,410f)
                        .lineTo(360f,410f)
                        .lineTo(360f,410f)
                        .lineTo(360f,410f)
                        .lineTo(360f,410f)
                        .lineTo(360f,410f)
                        .lineTo(360f,410f)
                        .lineTo(360f,410f)
                        .lineTo(360f,410f)
                        .lineTo(360f,410f)
                        .lineTo(360f,410f)
                        .lineTo(360f,410f)
                        .lineTo(360f,410f)
                        .lineTo(360f,410f)
                        .lineTo(360f,410f)
                        .lineTo(360f,410f)
                        .lineTo(360f,410f)
                        .lineTo(360f,410f)
                        .lineTo(360f,410f)
                        .lineTo(370f,810f)
                        .build()
                    gesture.dispatchGesture(gd)
                }
            }
        }
    }
}