package cn.wufou.auto.app

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import cn.wufou.auto.app.demo.DemoBotBox
import cn.wufou.auto.bot.BotClient
import cn.wufou.auto.core.MagicCore
import cn.wufou.auto.script.bot.JavaScriptBotBox
import kotlin.concurrent.thread

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        BotClient.envConfig().apply {
            this.setNotifyTitle("1")
            this.setNotifyText("2")
        }
        findViewById<View>(R.id.btn_test).setOnClickListener {
            BotClient.mount(DemoBotBox())
            BotClient.launch()
        }
    }


}