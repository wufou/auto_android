package cn.wufou.auto.script.runtime


interface IJavaScriptRuntime {

    /**
     * 注入到全局对象
     */
    fun injectObject(name: String, obj: Any)

    /**
     * 装载脚本
     */
    fun executeScript(script: String)

    /**
     * 关闭
     */
    fun close()

}