package cn.wufou.auto.script.runtime.v8

import cn.wufou.auto.core.annotation.JSMethod
import cn.wufou.auto.script.runtime.IJavaScriptRuntime
import com.eclipsesource.v8.JavaCallback
import com.eclipsesource.v8.V8
import com.eclipsesource.v8.V8Object
import com.eclipsesource.v8.V8Value
import java.lang.reflect.Array
import kotlin.reflect.KClass


class V8JavaScriptRuntime() : IJavaScriptRuntime {

    // 实际运行时环境
    private var runtime: V8? = null

    private val cache = mutableMapOf<String, Any>()

    /**
     * 注入全局对象
     */
    override fun injectObject(name: String, obj: Any) {
        cache[name] = obj
    }


    private fun <T : Any> inject(runtime: V8, name: String, obj: Any, clazz: KClass<T>) {
        // 1.反射成员方法
        val v8Object = V8Object(runtime)
        this.cache[name] = v8Object
        val methods = clazz.java.methods
        methods.map { method ->
            val jsMethod = method.getAnnotation(JSMethod::class.java)
            if (jsMethod != null) {
                val jsMethodName = jsMethod.name.ifEmpty {
                    method.name
                }
//                v8Object.registerJavaMethod(obj, method.name, jsMethodName, parameterType)
                v8Object.registerJavaMethod(JavaCallback { source, parameters ->
                    val paramCaches = mutableListOf<Any>()
                    val parameterType = method.parameterTypes
                    val params = parameterType.mapIndexed { index, clazz ->
                        val param = if (index == parameterType.size - 1) {
                            // 最后一个参数
                            if (method.isVarArgs) {
                                val size = parameters.length() - index
                                // 最后几个参数组合成数组
                                val array = Array.newInstance(String::class.java, size)
                                for (i in index until index + size) {
                                    clazz.componentType?.cast(parameters.get(i))?.let {
                                        paramCaches.add(it)
                                        Array.set(array, i - index, it)
                                    }
                                }
                                array
                            } else {
                                parameters.get(index)?.apply {
                                    paramCaches.add(this)
                                }
                            }
                        } else {
                            parameters.get(index)?.apply {
                                paramCaches.add(this)
                            }
                        }
                        param
                    }.toTypedArray()
                    val res = method.invoke(obj, *params)
                    paramCaches.forEach {
                        if (it is V8Value) {
                            it.close()
                        }
                    }
                    V8Utils.toV8Any(res, runtime)
                }, jsMethodName)
            }
        }
        v8Object.close()
    }

    override fun executeScript(script: String) {
        val runtime = V8.createV8Runtime()
        this.runtime = runtime
        this.cache.keys.forEach {
            val obj = this.cache[it] ?: return
            this.inject(runtime, it, obj, obj::class)
        }
        runtime.executeVoidScript(script)
    }

    override fun close() {
        runtime?.close()
    }


}