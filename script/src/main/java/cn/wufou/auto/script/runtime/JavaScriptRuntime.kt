package cn.wufou.auto.script.runtime

import cn.wufou.auto.script.lib.Console
import cn.wufou.auto.script.runtime.v8.V8JavaScriptRuntime

class JavaScriptRuntime {
    companion object {

        /**
         * 创建运行时
         */
        fun createRuntime(): IJavaScriptRuntime {
            return V8JavaScriptRuntime().apply {
                injectObject("console", Console())
            }
        }
    }
}