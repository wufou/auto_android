package cn.wufou.auto.script

import cn.wufou.auto.bot.BotClient
import cn.wufou.auto.script.bot.JavaScriptBotBox

object ScriptManager {

    fun execute(script:String){
        BotClient.mount(JavaScriptBotBox(script))
        BotClient.launch()
    }


}