package cn.wufou.auto.script.ws

import android.app.Service
import android.content.Intent
import android.os.IBinder

class ScriptWebSocketService : Service() {


    private val server = ScriptWebSocketServer()

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()
        server.start()
    }

    override fun onDestroy() {
        super.onDestroy()
        server.stop()
    }
}