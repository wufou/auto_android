package cn.wufou.auto.script.bot

import cn.wufou.auto.bot.Bot
import cn.wufou.auto.bot.context.BotContext
import cn.wufou.auto.script.runtime.JavaScriptRuntime
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class JavaScriptBot(private val script: String) : Bot {
    override suspend fun execute(context: BotContext) {
        val runtime = JavaScriptRuntime.createRuntime()
        withContext(Dispatchers.IO) {
            runtime.executeScript(script)
        }
    }
}