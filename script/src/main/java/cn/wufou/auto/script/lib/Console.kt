package cn.wufou.auto.script.lib

import android.util.Log
import cn.wufou.auto.core.annotation.JSMethod

/**
 * 控制台
 */
class Console {

    @JSMethod("log")
    fun log(text: String) {
        Log.i("[Console-LOG]", text.toString())
    }


}