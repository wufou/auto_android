package cn.wufou.auto.script.bot

import cn.wufou.auto.bot.Bot
import cn.wufou.auto.bot.BotBox

class JavaScriptBotBox(private val script: String) : BotBox {
    override fun read(): Bot {
        return JavaScriptBot(script)
    }
}