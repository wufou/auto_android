package cn.wufou.auto.script.ws

import cn.wufou.auto.bot.BotClient
import cn.wufou.auto.script.bot.JavaScriptBotBox
import org.java_websocket.WebSocket
import org.java_websocket.client.WebSocketClient
import org.java_websocket.handshake.ClientHandshake
import org.java_websocket.handshake.ServerHandshake
import org.java_websocket.server.WebSocketServer
import org.json.JSONObject
import java.io.ByteArrayInputStream
import java.lang.Exception
import java.net.InetSocketAddress
import java.net.URI

class ScriptWebSocketServer(port: Int = 9543) : WebSocketServer(InetSocketAddress(port)) {

    override fun onOpen(conn: WebSocket, handshake: ClientHandshake) {
        println("客户端连接")
    }

    override fun onClose(conn: WebSocket, code: Int, reason: String?, remote: Boolean) {
        println("客户端已断开")
    }

    override fun onMessage(conn: WebSocket, message: String) {
        println("客服端消息:$message")
        val msgBody = try {
            JSONObject(message)
        } catch (e: Exception) {
            conn.send("非法调用！")
            return
        }
        when (msgBody.getString("name")) {
            "run script" -> {
                // 执行脚本
                val script = msgBody.getString("script")
                // 加载执行脚本
                BotClient.mount(JavaScriptBotBox(script))
                BotClient.launch()
            }
        }

    }

    override fun onError(conn: WebSocket?, ex: Exception?) {
        println("服务器异常")
    }

    override fun onStart() {
        println("服务器已启动")
        val uri = URI("ws://192.168.1.135:9543")
        object : WebSocketClient(uri) {
            override fun onOpen(handshakedata: ServerHandshake?) {
                println()
                send("hello")
            }

            override fun onMessage(message: String?) {
                println()
            }

            override fun onClose(code: Int, reason: String?, remote: Boolean) {
                println()
            }

            override fun onError(ex: Exception?) {
                println()
            }

        }.connect()
    }
}