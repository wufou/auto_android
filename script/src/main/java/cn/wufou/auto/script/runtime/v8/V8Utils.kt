package cn.wufou.auto.script.runtime.v8

import cn.wufou.auto.core.annotation.JSMethod
import cn.wufou.auto.core.annotation.JSProperty
import com.eclipsesource.v8.V8
import com.eclipsesource.v8.V8Array
import com.eclipsesource.v8.V8Object
import com.eclipsesource.v8.V8Value
import com.eclipsesource.v8.utils.V8ObjectUtils

/**
 * V8对象转换工具
 */
object V8Utils {

    /**
     * 转化位v8可识别对象
     */
    fun toV8Any(any: Any?, runtime: V8): Any? {
        return any?.run {
            when (this) {
                /* 有些类型不需要转化 */
                is Byte,
                is Char,
                is Short,
                is Int,
                is Long,
                is Boolean,
                is Float,
                is Double,
                is String,
                is V8Value -> return this
                /* 列表要转成v8专用的Array类型 */
                is List<*> -> return this.toV8Array(runtime)
                // todo 数组类型也需要处理
                /* 对象转化成v8可识别类型 */
                else -> this.toV8Object(runtime)
            }
        }
    }

    /**
     * 把一个对象转化为v8对象
     */
    fun Any.toV8Object(v8: V8): V8Object {
        val obj = V8Object(v8)
        // 注入属性
        val fields = this::class.java.declaredFields
        this::class.java.fields
        fields.map { field ->
            val jsField = field.getDeclaredAnnotation(JSProperty::class.java)
            if (jsField != null) {
                val jsFieldName = jsField.name.ifEmpty {
                    jsField.name
                }
                field.isAccessible = true
                when (field.type) {
                    Byte::class.java,
                    Char::class.java,
                    Short::class.java,
                    Int::class.java,
                    Long::class.java -> obj.add(jsFieldName, field.getInt(this))
                    Boolean::class.java -> obj.add(jsFieldName, field.getBoolean(this))
                    Float::class.java,
                    Double::class.java -> obj.add(jsFieldName, field.getDouble(this))
                    String::class.java -> obj.add(jsFieldName, field.get(this)?.toString())
                    else -> obj.add(jsFieldName, field.get(this)?.toV8Object(v8))
                }
            }
        }
        // 注入函数
        val methods = this::class.java.methods
        methods.map { method ->
            val jsMethod = method.getAnnotation(JSMethod::class.java)
            if (jsMethod != null) {
                val jsMethodName = jsMethod.name.ifEmpty {
                    method.name
                }
                val parameterType = method.parameterTypes
                obj.registerJavaMethod(this, method.name, jsMethodName, parameterType)
            }
        }
        return obj
    }


    fun List<*>.toV8Array(v8: V8): V8Array {
        return V8ObjectUtils.toV8Array(v8, this.map {
            it?.let { it1 -> toV8Any(it1, v8) }
        })
    }

    fun Array<Any>.toV8Array(v8: V8): V8Array {
        return V8ObjectUtils.toV8Array(v8, this.map {
            toV8Any(it, v8)
        })
    }


}