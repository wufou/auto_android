package cn.wufou.auto.core

import android.accessibilityservice.AccessibilityService
import android.app.Activity
import android.app.Application
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import cn.wufou.auto.core.magic.MagicClient

internal object EnvSettings {

    // 无障碍服务的期用状态
    private val liveAccessibilityEnabled = MutableLiveData<Boolean>(false)
    private val liveOverlayEnabled = MutableLiveData<Boolean>(false)
    private var activityLifecycleCallbacks: Application.ActivityLifecycleCallbacks? = null

    /**
     * 生成一个无障碍服务启用状态的LiveData对象
     */
    fun createLiveAccessibilityEnabled(context: Context): LiveData<Boolean> {
        bindRefreshSource(context)
        return MediatorLiveData<Boolean>().apply {
            val enabled = hasAccessibilityEnabled(context)
            if (enabled != this.value) {
                liveAccessibilityEnabled.value = enabled
            }
            this.addSource(liveAccessibilityEnabled) {
                this.postValue(it ?: false)
            }
        }
    }

    /**
     * 生成一个绘制在其他应用之上启用状态的LiveData对象
     */
    fun createLiveDrawOverlaysEnabled(context: Context): LiveData<Boolean> {
        bindRefreshSource(context)
        return MediatorLiveData<Boolean>().apply {
            val enabled = hasDrawOverlaysEnabled(context)
            if (enabled != this.value) {
                liveOverlayEnabled.value = enabled
            }
            this.addSource(liveOverlayEnabled) {
                this.postValue(it ?: false)
            }
        }
    }

    /**
     * 绑定
     */
    private fun bindRefreshSource(context: Context) {
        if (activityLifecycleCallbacks == null) {
            if (context is Application) {
                val activityLifecycleCallbacks = object : CommonActivityLifecycleCallbacks() {
                    override fun onActivityResumed(activity: Activity) {
                        liveAccessibilityEnabled.value = hasAccessibilityEnabled(activity)
                        liveOverlayEnabled.value = hasDrawOverlaysEnabled(activity)
                    }
                }
                context.registerActivityLifecycleCallbacks(activityLifecycleCallbacks)
                EnvSettings.activityLifecycleCallbacks = activityLifecycleCallbacks
            }
        }
    }


    fun hasAccessibilityEnabled(context: Context): Boolean {
        return hasAccessibilityEnabled(context, MagicClient::class.java)
    }

    fun startAccessibilitySetting(context: Context): Boolean {
        if (!hasAccessibilityEnabled(context, MagicClient::class.java)) {
            val intent = Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS)
            if(context !is Activity){
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            }
            context.startActivity(intent)
            return true
        } else {
            return false
        }
    }


    fun startOverlayPermissionSetting(context: Context) {
        if (!hasDrawOverlaysEnabled(context)) {
            val intent = Intent()
            intent.action = Settings.ACTION_MANAGE_OVERLAY_PERMISSION;
            context.startActivity(intent)
        }
    }

    /**
     * 用户是否允许了绘制在其他应用之上
     */
    fun hasDrawOverlaysEnabled(context: Context): Boolean {
        return Settings.canDrawOverlays(context)
    }

    fun stopDrawOverlaysEnabled() {

    }


    /**
     * 用户是否启用了无障碍服务。如果有多个服务即使只有一个启动也会返回true
     */
    fun hasAccessibilityEnabled(context: Context, clz: Class<out AccessibilityService>): Boolean {
        if (Settings.Secure.getString(context.contentResolver, Settings.Secure.ACCESSIBILITY_ENABLED) == "0") {
            // 这个时候没有任何无障碍服务被启动 直接忽略
            return false
        }
        val services = Settings.Secure.getString(context.contentResolver, Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES)
        val serviceArray = services.split(":").filter {
            it.isNotEmpty()
        }
        return serviceArray.any {
            val array = it.split("/")
            if (array.size != 2) {
                return@any false
            }
            val packageName = array[0]
            val serviceName = array[1]
            val name = ComponentName.createRelative(packageName, serviceName)
            val target = ComponentName.createRelative(context, clz.name)
            name == target
        }
    }

    private open class CommonActivityLifecycleCallbacks : Application.ActivityLifecycleCallbacks {
        open override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
        }

        open override fun onActivityStarted(activity: Activity) {
        }

        open override fun onActivityResumed(activity: Activity) {
        }

        open override fun onActivityPaused(activity: Activity) {
        }

        open override fun onActivityStopped(activity: Activity) {
        }

        open override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {
        }

        open override fun onActivityDestroyed(activity: Activity) {
        }

    }

}