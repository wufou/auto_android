package cn.wufou.auto.core.exception

import java.lang.RuntimeException

class ServiceNotFoundException(message: String? = "服务未运行") : RuntimeException(message) {
}