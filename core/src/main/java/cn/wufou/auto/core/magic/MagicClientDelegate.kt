package cn.wufou.auto.core.magic

import android.accessibilityservice.GestureDescription
import android.os.Handler
import android.view.accessibility.AccessibilityEvent
import android.view.accessibility.AccessibilityNodeInfo
import android.view.accessibility.AccessibilityWindowInfo
import cn.wufou.auto.core.accessibility.AccessibilityDelegate
import cn.wufou.auto.core.accessibility.AccessibilityDelegateOwner
import cn.wufou.auto.core.accessibility.AccessibilityService
import cn.wufou.auto.core.exception.ServiceNotFoundException
import java.lang.ref.WeakReference

/**
 * 对服务的封装处理
 */
internal object MagicClientDelegate : IMagicClient {

    // 缓存的服务实例
    private var service = WeakReference<MagicClient>(null)


    init {
        // 全局注册一个服务代理
        AccessibilityDelegateOwner.addDelegate("MagicServiceDelegate", object : AccessibilityDelegate.EmptyAccessibilityDelegate() {
            override fun onServiceConnected(accessibilityService: AccessibilityService) {
                if (accessibilityService is MagicClient) {
                    service = WeakReference(accessibilityService)
                }
            }

            override fun onDestroy(accessibilityService: AccessibilityService) {
                if (accessibilityService is MagicClient) {
                    service.clear()
                }
            }

            override fun onAccessibilityEvent(accessibilityService: AccessibilityService, event: AccessibilityEvent) {
                super.onAccessibilityEvent(accessibilityService, event)
                if (accessibilityService is MagicClient) {
                }
            }
        })
    }

    /**
     * 确保服务已启用
     */
    private fun checkService(): MagicClient {
        return service.get() ?: throw ServiceNotFoundException("MagicService未启动")
    }

    override fun getRootNodeInfoInActiveWindow(): AccessibilityNodeInfo? {
        return checkService().run {
            this.getRootNodeInfoInActiveWindow()
        }
    }

    override fun getWindows(): List<AccessibilityWindowInfo> {
        return checkService().run {
            this.windows
        }
    }

    override fun dispatchGestureEvent(gesture: GestureDescription, callback: android.accessibilityservice.AccessibilityService.GestureResultCallback?, handler: Handler?): Boolean {
        return checkService().run {
            this.dispatchGesture(gesture, callback, handler)
        }
    }

    override fun performGlobalAction(action: Int): Boolean {
        return checkService().run {
            this.performGlobalAction(action)
        }
    }
}