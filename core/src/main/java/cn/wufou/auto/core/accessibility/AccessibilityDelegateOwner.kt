package cn.wufou.auto.core.accessibility

import android.view.accessibility.AccessibilityEvent
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.LifecycleOwner
import java.lang.ref.WeakReference

internal object AccessibilityDelegateOwner {

    private val delegateHolder: MutableMap<String, AccessibilityDelegate> = mutableMapOf()

    private var service: WeakReference<AccessibilityService> = WeakReference(null)

    internal fun addDelegate(name: String, delegate: AccessibilityDelegate) {
        delegateHolder[name] = delegate
    }

    internal fun removeDelegate(name: String) {
        delegateHolder.remove(name)
    }

    fun getService(): AccessibilityService? {
        return service.get()
    }

    fun attach(service: AccessibilityService) {
        this.service = WeakReference(service)
        service.lifecycle.addObserver(object : LifecycleEventObserver {
            override fun onStateChanged(source: LifecycleOwner, event: Lifecycle.Event) {
                if (event == Lifecycle.Event.ON_DESTROY) {
                    this@AccessibilityDelegateOwner.service.clear()
                    source.lifecycle.removeObserver(this)
                }
            }
        })
    }


    fun dispatchAccessibilityEvent(accessibilityService: AccessibilityService, event: AccessibilityEvent) {
        for (delegate in delegateHolder.values) {
            delegate.onAccessibilityEvent(accessibilityService, event)
        }
    }

    fun dispatchInterrupt(accessibilityService: AccessibilityService) {
        for (delegate in delegateHolder.values) {
            delegate.onInterrupt(accessibilityService)
        }
    }

    fun dispatchServiceConnected(accessibilityService: AccessibilityService) {
        for (delegate in delegateHolder.values) {
            delegate.onServiceConnected(accessibilityService)
        }
    }

    fun dispatchDestroy(accessibilityService: AccessibilityService) {
        for (delegate in delegateHolder.values) {
            delegate.onDestroy(accessibilityService)
        }
    }

}