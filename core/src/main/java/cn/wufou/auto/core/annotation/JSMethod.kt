package cn.wufou.auto.core.annotation

@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
annotation class JSMethod(val name: String = "")
