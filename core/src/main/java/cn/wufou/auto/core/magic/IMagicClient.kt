package cn.wufou.auto.core.magic

import android.accessibilityservice.AccessibilityService.GestureResultCallback
import android.accessibilityservice.GestureDescription
import android.os.Handler
import android.view.accessibility.AccessibilityEvent
import android.view.accessibility.AccessibilityNodeInfo
import android.view.accessibility.AccessibilityWindowInfo

/**
 * 定义无障碍的核心方法
 */
interface IMagicClient {


    /**
     * 获取激活窗口根节点
     */
    fun getRootNodeInfoInActiveWindow(): AccessibilityNodeInfo?

    /**
     * 获取窗口信息
     */
    fun getWindows(): List<AccessibilityWindowInfo>

    /**
     * 派遣手势事件
     */
    fun dispatchGestureEvent(gesture: GestureDescription, callback: GestureResultCallback?, handler: Handler?): Boolean


    /**
     * 执行一些全局事件
     */
    fun performGlobalAction(action: Int): Boolean
}