package cn.wufou.auto.core.magic

import android.accessibilityservice.AccessibilityServiceInfo
import android.accessibilityservice.GestureDescription
import android.os.Handler
import android.view.accessibility.AccessibilityNodeInfo
import androidx.lifecycle.LifecycleOwner
import cn.wufou.auto.core.accessibility.AccessibilityService


/**
 * 定义UI事件、状态的交互服务
 *  * 接受系统发出的基于Window的视图变化
 *  * 发送基于无障碍功能的事件模拟
 */
class MagicClient : AccessibilityService(), IMagicClient, LifecycleOwner {

    companion object {
        const val NOTIFICATION_ID = 1501
        const val CHANNEL_AUTO_ID = "auto"
        const val CHANNEL_AUTO_NAME = "自动化"
    }


    override fun onCreate() {
        super.onCreate()
        MagicNotification.startForegroundNotification(this)// 注册通知
    }

    override fun onServiceConnected() {
        super.onServiceConnected()
        val serviceInfo = this.serviceInfo
        serviceInfo.feedbackType = AccessibilityServiceInfo.FEEDBACK_SPOKEN
        this.serviceInfo = serviceInfo
    }


    override fun onDestroy() {
        super.onDestroy()
        // 移除通知
        MagicNotification.stopForegroundNotification(this)
    }

    override fun getRootNodeInfoInActiveWindow(): AccessibilityNodeInfo? {
        return rootInActiveWindow
    }

    override fun dispatchGestureEvent(gesture: GestureDescription, callback: GestureResultCallback?, handler: Handler?): Boolean {
        return super.dispatchGesture(gesture, callback, handler)
    }

}