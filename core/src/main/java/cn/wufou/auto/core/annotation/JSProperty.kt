package cn.wufou.auto.core.annotation

@Target(
    AnnotationTarget.FIELD, AnnotationTarget.PROPERTY_GETTER,AnnotationTarget.VALUE_PARAMETER
)
@Retention(AnnotationRetention.RUNTIME)
annotation class JSProperty(val name: String = "")
