package cn.wufou.auto.core.magic

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.content.pm.ServiceInfo
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.app.ServiceCompat
import cn.wufou.auto.core.MagicCore

/**
 * 通知样式
 */
internal object MagicNotification {

    // 标题
    private var title: CharSequence = "Magic"

    // 内容
    private var text: CharSequence = "Magic服务运行中"

    // 图标
    private var smallIcon: Int? = null
    private var pi: PendingIntent? = null

    /**
     * 设置通知标题
     */
    fun setTitle(title: CharSequence) {
        this.title = title
    }

    /**
     * 设置通知内容
     */
    fun setText(text: CharSequence) {
        this.text = text
    }

    /**
     * 设置通知图标
     */
    fun setSmallIcon(iconRes: Int) {
        this.smallIcon = iconRes
    }

    /**
     * 设置通知单击意图
     */
    fun setContentActivityIntent(intent: Intent) {
        this.pi = PendingIntent.getActivity(MagicCore.getApplication(), 1, intent, PendingIntent.FLAG_IMMUTABLE)
    }

    /**
     * 启动通知
     */
    fun startForegroundNotification(context: Service) {
        val nmc = NotificationManagerCompat.from(context)
        val channel = NotificationChannel(MagicClient.CHANNEL_AUTO_ID, MagicClient.CHANNEL_AUTO_NAME, NotificationManager.IMPORTANCE_DEFAULT)
        nmc.createNotificationChannel(channel)
        val notification = NotificationCompat.Builder(context, MagicClient.CHANNEL_AUTO_ID)
            .setSmallIcon(smallIcon ?: context.applicationInfo.icon)
            .setContentTitle(title)
            .setContentText(text)
            .setPriority(NotificationCompat.PRIORITY_LOW)
            .setContentIntent(this.pi)
            .build()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            context.startForeground(MagicClient.NOTIFICATION_ID, notification, ServiceInfo.FOREGROUND_SERVICE_TYPE_CONNECTED_DEVICE)
        } else {
            context.startForeground(MagicClient.NOTIFICATION_ID, notification)
        }
    }

    /**
     * 关闭通知
     */
    fun stopForegroundNotification(context: Service) {
        ServiceCompat.stopForeground(context, ServiceCompat.STOP_FOREGROUND_REMOVE)
    }
}