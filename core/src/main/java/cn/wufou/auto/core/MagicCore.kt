package cn.wufou.auto.core

import android.app.Application
import android.content.Context
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import cn.wufou.auto.core.accessibility.AccessibilityDelegate
import cn.wufou.auto.core.accessibility.AccessibilityDelegateOwner
import cn.wufou.auto.core.accessibility.AccessibilityService
import cn.wufou.auto.core.magic.IMagicClient
import cn.wufou.auto.core.magic.MagicNotification
import cn.wufou.auto.core.magic.MagicClient
import cn.wufou.auto.core.magic.MagicClientDelegate
import kotlinx.coroutines.channels.ticker
import java.lang.RuntimeException
import java.lang.ref.WeakReference

/**
 * <p>Magic核心功能管理实例</p>
 */
object MagicCore : LifecycleOwner {


    private var service = WeakReference<AccessibilityService>(null)
    private val CLIENT_DELEGATE: IMagicClient = MagicClientDelegate
    private var application: Application? = null

    /**
     * 应用上下文
     */
    private val mLifecycleRegistry = LifecycleRegistry(this)

    init {
        // 全局注册一个服务代理
        AccessibilityDelegateOwner.addDelegate("MagicCore", object : AccessibilityDelegate.EmptyAccessibilityDelegate() {
            override fun onServiceConnected(accessibilityService: AccessibilityService) {
                if (accessibilityService is MagicClient) {
                    service = WeakReference(accessibilityService)
                    mLifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_CREATE)
                    mLifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_START)
                }
            }

            override fun onDestroy(accessibilityService: AccessibilityService) {
                if (accessibilityService is MagicClient) {
                    service.clear()
                    mLifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_STOP)
                    mLifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_DESTROY)
                }
            }
        })
    }

    /**
     * 获取服务的客户端实例
     */
    fun getMagicClient(): IMagicClient {
        return CLIENT_DELEGATE
    }


    /**
     * 对外提供一个可观察的生命周期
     */
    override fun getLifecycle(): Lifecycle {
        return mLifecycleRegistry
    }

    internal fun attach(application: Application) {
        this.application = application
    }


    /**
     * 设置前台通知的标题
     */
    fun setForegroundNotifyTitle(title: CharSequence) {
        MagicNotification.setTitle(title)
    }

    /**
     * 设置前台通知的文本内容
     */
    fun setForegroundNotifyText(text: CharSequence) {
        MagicNotification.setText(text)
    }

    /**
     * 设置前台通知的小图标
     */
    fun setForegroundNotifySmallIcon(iconRes: Int) {
        MagicNotification.setSmallIcon(iconRes)
    }

    /**
     * 功能是否已启用
     * start()后立即调用可能会返回false，服务连接需要一点时间
     * @return true 服务已启动 false 服务未启动，也有可能正在启动中
     */
    fun isEnable(): Boolean {
        return mLifecycleRegistry.currentState >= Lifecycle.State.CREATED
    }

    /**
     * 尝试去启动引擎
     */
    fun start(): Boolean {
        if (isEnable()) {
            return false
        }
        return EnvSettings.startAccessibilitySetting(getApplication())
    }

    /**
     * 停止服务
     */
    fun stop(): Boolean {
        val service = this.service.get() ?: return false
        service.disableSelf()
        return true
    }


    /**
     * 获取应用上下文
     */
    fun getApplication(): Application {
        return application ?: throw RuntimeException("Magic尚未初始化")
    }

    /**
     * 获取服务的上下文
     * 不要持有这个上下文！ 只有在某些特殊场景下需要使用这个实例，脚本开发中基本不会用到
     */
    fun getServiceContext(): Context? {
        return AccessibilityDelegateOwner.getService()
    }


}