package cn.wufou.auto.core.accessibility

import android.view.accessibility.AccessibilityEvent
import androidx.annotation.CallSuper
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ServiceLifecycleDispatcher

open class AccessibilityService : android.accessibilityservice.AccessibilityService(), LifecycleOwner {


    /**
     *  为了执行某些依赖生命周期的操作，需要service支持lifecycle
     */
    private val mDispatcher by lazy { ServiceLifecycleDispatcher(this) }

    @CallSuper
    override fun onCreate() {
        super.onCreate()
        mDispatcher.onServicePreSuperOnCreate()
        AccessibilityDelegateOwner.attach(this)
    }

    @CallSuper
    override fun onServiceConnected() {
        mDispatcher.onServicePreSuperOnStart()
        super.onServiceConnected()
        AccessibilityDelegateOwner.dispatchServiceConnected(this)
    }


    @CallSuper
    override fun onDestroy() {
        mDispatcher.onServicePreSuperOnDestroy()
        super.onDestroy()
        AccessibilityDelegateOwner.dispatchDestroy(this)
    }

    @CallSuper
    override fun onAccessibilityEvent(event: AccessibilityEvent) {
        AccessibilityDelegateOwner.dispatchAccessibilityEvent(this, event)
    }

    @CallSuper
    override fun onInterrupt() {
        AccessibilityDelegateOwner.dispatchInterrupt(this)
    }

    override fun getLifecycle(): Lifecycle {
        return mDispatcher.lifecycle
    }
}