package cn.wufou.auto.core.accessibility

import android.view.accessibility.AccessibilityEvent

internal interface AccessibilityDelegate {

    fun onServiceConnected(accessibilityService: AccessibilityService)

    fun onDestroy(accessibilityService: AccessibilityService)

    fun onAccessibilityEvent(accessibilityService: AccessibilityService, event: AccessibilityEvent)

    fun onInterrupt(accessibilityService: AccessibilityService)


    open class EmptyAccessibilityDelegate : AccessibilityDelegate {
        override fun onServiceConnected(accessibilityService: AccessibilityService) {
        }

        override fun onDestroy(accessibilityService: AccessibilityService) {
        }

        override fun onAccessibilityEvent(accessibilityService: AccessibilityService, event: AccessibilityEvent) {
        }

        override fun onInterrupt(accessibilityService: AccessibilityService) {
        }

    }

}